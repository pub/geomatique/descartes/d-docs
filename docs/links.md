# Liens utiles
---------------

**Salon public sur Tchap pour les échanges sur le composant Descartes**: [https://tchap.gouv.fr/#/room/#Descartes:agent.dev-durable.tchap.gouv.fr](https://tchap.gouv.fr/#/room/#Descartes:agent.dev-durable.tchap.gouv.fr)

**Application de démonstation**: [https://descartes.din.developpement-durable.gouv.fr/demo](https://descartes.din.developpement-durable.gouv.fr/demo)

**Fonctionnalités de visualisation** (Module D-Map):

* Exemples: [https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-map](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-map)
* Code source: [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-map](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-map)

**Fonctionnalités d'édition et d'annotations** (Module D-EditMap):

* Exemples: [https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-editmap](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-editmap)
* Code source: [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-editmap](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-editmap)

**Fonctionnalités d'impression** (Module D-InkMap):

* Exemples: [https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-inkmap](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-inkmap)
* Code source: [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-inkmap](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-inkmap)

<span style="color:red;">(bêta)</span>**Fonctionnalités utilisant des services de la Géoplateforme de l'IGN** (Module D-GeoPlateforme):

* Exemples: [https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-geoplateforme](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-geoplateforme)
* Code source: [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-geoplateforme](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-geoplateforme)

**Supervision des services Web** :

* Accès au suivi de la supervision (intranet): [http://psin.supervision.e2.rie.gouv.fr/portails/MonApplication.php?application=DESCARTES](http://psin.supervision.e2.rie.gouv.fr/portails/MonApplication.php?application=DESCARTES)
* Accès aux remontées d'alertes de supervision sur Tchap (sur invitation): #Descartes_Alertes_Supervision

**Autres liens** :

* Tous les exemples détaillés (D-Recette): [https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette)
* Page swagger API Doc des services Web (D-WSMap) : [https://descartes-server.din.developpement-durable.gouv.fr/services/api/docs.html](https://descartes-server.din.developpement-durable.gouv.fr/services/api/docs.html)
* Accès à l'ensemble des modules Descartes sur Gitlab: [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes)

