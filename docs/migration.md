# Migrations
---------------

## Migration v5.X.X vers v6.0.0
---------------

#### Montée de version des librairies utilisées pour l'interface graphique des composants

Passage de Bootstrap 3.3.7 à **4.1.3**

Passage de Jquery 1.12.4 à **3.3.1**

!!! warning "ATTENTION"
	Les applications métiers doivent migrer leur interface graphique (IHM) pour utiliser ces versions de Bootstrap et Jquery.

#### Mise à disposition de 4 paquets NPM

La librairie Descartes est maintenant constituée de 4 modules :

* module **d-map** (fonctionnalités de visualisation) : [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-map](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-map)
	
* module **d-editmap** (fonctionnalité de la gestion des objets géographiques et d'annotation) : [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-editmap](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-editmap)
	
* module **d-inkmap** (les fonctionnalités d'impression) : [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-inkmap](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-inkmap)

* <span style="color:red;">(bêta)</span> module **d-geoplateforme** (fonctionnalités utilisant des services de la Géoplateforme de l'IGN) : [https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-geoplatefofme](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/d-geoplateforme)


Ces modules sont disponibles sous la forme des paquets NPM. Les modules de la librairie sont sur : 

* l'entrepôt NPM du Gitlab Ministère : 
	
	```
	 @descartes:registry=https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm
	```

* le gestionnaire des paquets du Gitlab Ministère (téléchargement manuel) : 

	[https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages)

* l'entrepôt public NPM (https://registry.npmjs.org) pour les versions diffusées "grand public" à accès CDN : 

    ```
	https://cdn.jsdelivr.net/npm/@descartes/d-xxx@X.X.X/...
	```
	

#### Mise à disposition de fichiers "required" regroupant l'ensemble des librairies tierces nécessaires au bon fonctionnement de la librairie principale

Dans chaque module (d-map, d-editmap et d-inkmap), des fichiers ( xxx-required.js et xx-required.css) regroupant l'ensemble des librairies nécessaires au bon fonctionnement sont fournis dans les paquets NPM.
Ils regroupent par exemple :
    
* la bibliothèque OpenLayers dans sa version 4.6.5 
	
* la bibliothèque OL-ext dans sa version 1.1.3
	
* la bibliothèque JSTS dans sa version 1.6.0
	
* la bibliothèque Proj4 dans sa version 2.4.4

* la bibliothèque GeoStyler dans sa version 0.17.4
	
* pour l’interface graphique des composants:
	
	* la bibliothèque Bootstrap dans sa version 4.1.3 (fichiers bootstrap.min.js, bootstrap-table.min.js, bootstrap-slider.min.js et bootstrap.min.css, bootstrap-table.min.css, bootstrap-slider.min.css)
					
	* la bibliothèque jQuery dans sa version 3.3.1 (fichiers jquery.min.js, jquery-ui.min.js, jquery-ui.min.css)
					
	* la bibliothèque jsTree dans sa version 3.3.4 (fichiers jstree.min.js, style.min.css)

* pour l'impression:

	* la bibliothèque InkMap dans sa version 1.3.0
	
* pour l'utilisation des services de la Géoplateforme de l'IGN <span style="color:red;">(bêta)</span> 

	* la bibliothèque geoportal-access-lib dans sa version 3.4.2
	
	* la bibliothèque chart.js dans sa version 4.4.3

La déclaration de la librairie se fait maintenant de la façon suivante:

```
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="d-map/dist/vendor/d-map-required.css">
	<link rel="stylesheet" type="text/css" href="d-map/dist/d-map.css"/>
	<link rel="stylesheet" type="text/css" href="d-inkmap/dist/vendor/d-inkmap-required.css">
	<link rel="stylesheet" type="text/css" href="d-inkmap/dist/d-inkmap.css"/>
	<link rel="stylesheet" type="text/css" href="d-editmap/dist/d-editmap.css"/>
	<link rel="stylesheet" type="text/css" href="d-geoplateforme/dist/d-geoplateforme.css"/>
	<script type="text/javascript" src="d-map/dist/vendor/d-map-required.js"></script>
	<script type="text/javascript" src="d-map/dist/d-map.js"></script>
	<script type="text/javascript" src="d-inkmap/dist/vendor/d-inkmap-required.js"></script>
	<script type="text/javascript" src="d-inkmap/dist/d-inkmap.js"></script>
	<script type="text/javascript" src="d-editmap/dist/d-editmap.js"></script>
	<script type="text/javascript" src="d-geoplateforme/dist/vendor/d-geoplateforme-required.js"></script>
	<script type="text/javascript" src="d-geoplateforme/dist/d-geoplateforme.js"></script>
</head>
</html>
```

#### Chargement automatique des définitions de Proj4

L'ajout des définitions se fait maintenant automatiquement au chargement des modules Descartes.

La déclaration des définitions des projections n'est donc plus nécessaire :

Exemple
```
proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
```

!!! note "Remarque"
	Les laisser au niveau du code de l'application métier ne posera aucun problème. Il n'est donc pas nécessaire de redéployer une application métier pour les enlever.


#### Mise à disposition des services centralisés sous la forme de services web (beta)

!!! warning "[beta] Services Web - En cours de qualification"
	Ces services sont disponibles 24/24, 7/7 et maintenus par MSP/DS/GSG.
	
	Ils sont en cours de qualifiaction dans l'infrastructure cloud eco du Ministère : mise en place de plusieurs noeuds et d'un répartiteur de charge.
	
	Ils peuvent être utilisés mais nous ne pouvons pour l'instant pas garantir la qualité de service (performance et disponibilité).
	
	Si vous rencontrez des problèmes de performance, vous avez la possibilité de palier à ce problème en déployant vos propres composants côté serveur : nous pouvons vous fournir une image docker (ou un war) de ces services (Documentation en cours de rédaction).

Jusqu'à présent, des services centralisés disponibles pour des serveurs d'applications (JAVA/PHP) étaient disponibles et devaient être intégrés dans les applications métiers.

Depuis cette version 6, ils sont accessibles directement par des Services Web centralisés (hébergement au centre serveur du MTE) et la librairie JavaScript est paramétrée pour automatiquement les utiliser.

Certains paramètres sont maintenant préconfigurés dès le chargement des modules.

Il n'est plus nécessaire de renseigner les éléments suivants: 
```
	Descartes.GEOREF_SERVER = "xxxx";
	Descartes.FEATUREINFO_SERVER = "xxxx";
	Descartes.FEATURE_SERVER = "xxxx";
	Descartes.EXPORT_PNG_SERVER = "xxxx";
	Descartes.EXPORT_PDF_SERVER = "xxxx";
	Descartes.EXPORT_CSV_SERVER = "xxxx";
	Descartes.CONTEXT_MANAGER_SERVER = "xxxx";
	Descartes.PROXY_SERVER = "xxxx";
```

!!! note "Remarque"
	Comme les précédentes versions, il est toujours possible de spécifier ces éléments en indiquant vos propres services embarqués dans votre application métier.
	
	Si vous voulez continuer à utiliser les services embarqués (au lieu des nouveaux Services Web mis à disposition), il faudra renseigner ces paramètres.

