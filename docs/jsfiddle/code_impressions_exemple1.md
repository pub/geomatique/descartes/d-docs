<form action="https://jsfiddle.net/api/post/library/pure" method="post" target="_blank">
    <a href="#" onclick="this.closest('form').submit(); return false;">
      <svg width="48" height="40" stroke="#2E71FF" style="vertical-align: middle;">
        <g stroke-width="1.5" fill="none" fill-rule="evenodd">
        <path d="M23.4888889,20.543316 C21.4404656,18.4187374 19.0750303,15.6666667 16.4832014,15.6666667 C13.8721947,15.6666667 11.7555556,17.6366138 11.7555556,20.0666667 C11.7555556,22.4967196 13.8721947,24.4666667 16.4832014,24.4666667 C18.8347252,24.4666667 19.9845474,23.0125628 20.6429148,22.312473" stroke-linecap="round"></path>
        <path d="M22.5111111,19.5900174 C24.5595344,21.7145959 26.9249697,24.4666667 29.5167986,24.4666667 C32.1278053,24.4666667 34.2444444,22.4967196 34.2444444,20.0666667 C34.2444444,17.6366138 32.1278053,15.6666667 29.5167986,15.6666667 C27.1652748,15.6666667 26.0154526,17.1207706 25.3570852,17.8208603" stroke-linecap="round"></path>
        <path d="M45,22.7331459 C45,19.1499462 42.7950446,16.079593 39.6628004,14.7835315 C39.6774469,14.5246474 39.7003932,14.2674038 39.7003932,14.0035978 C39.7003932,6.82243304 33.8412885,1 26.611593,1 C21.3985635,1 16.9102123,4.03409627 14.8051788,8.41527616 C13.7828502,7.62878013 12.503719,7.15547161 11.1134367,7.15547161 C7.77825654,7.15547161 5.07450503,9.84159999 5.07450503,13.1544315 C5.07450503,13.7760488 5.16938207,14.3779791 5.3477444,14.9418479 C2.74863428,16.4787471 1,19.2867709 1,22.5105187 C1,27.3287502 4.89630545,31.2367856 9.72803666,31.31094 L36.3341301,31.3109406 C41.1201312,31.3406346 45,27.4870665 45,22.7331459 L45,22.7331459 Z" stroke-linejoin="round"></path>
      </g>
      </svg>
      Tester en ligne sur JSFiddle
    </a>

    <input type="hidden" name="wrap" value="b">
    <input type="hidden" name="html" value="    
<!DOCTYPE html>
<html>
    <head>
        <meta charset=&quot;utf-8&quot;>
        <link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-map/dist/vendor/d-map-required.css&quot;>
        <link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-map/dist/css/d-map.css&quot;>
		<link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-inkmap/dist/vendor/d-inkmap-required.css&quot;>
        <link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-inkmap/dist/css/d-inkmap.css&quot;>

        <script src=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-map/dist/vendor/d-map-required.js&quot;></script>
        <script src=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-map/dist/d-map.js&quot;></script>
		<script src=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-inkmap/dist/vendor/d-inkmap-required.js&quot;></script>
        <script src=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/node_modules/@descartes/d-inkmap/dist/d-inkmap.js&quot;></script>
        <script type=&quot;text/javascript&quot; src=&quot;https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/test/d-recette/tests_fcts_visu_default/couches_groupes.js&quot;></script> 
    </head>
    <body onload=&quot;chargementCarte();&quot;>
       <div class=&quot;container-fluid&quot;>
            <div class=&quot;row&quot;>
                <div class=&quot;col-md-4 col-lg-3&quot;>
					<div id=&quot;layersTree&quot;></div>
					<div id=&quot;impressionPdf&quot;></div>
                </div>
                <div class=&quot;col-md-8 col-lg-9&quot;>
				    <div id=&quot;annotationsToolBar&quot;></div>
					<div id=&quot;map&quot;></div>
                </div>
            </div>
        </div>
    </body>
</html>




">
    <input type="hidden" name="js" value="
function chargementCarte() {
        chargeCouchesGroupes();

        var contenuCarte = new Descartes.MapContent();
		contenuCarte.addItem(coucheFer);
        contenuCarte.addItem(coucheEau);
        contenuCarte.addItem(coucheBati);
        contenuCarte.addItem(coucheNature);
		
        var bounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
        var projection = 'EPSG:2154';
		
        // Construction de la carte
        var carte = new Descartes.Map.ContinuousScalesMap(
                    'map',
                    contenuCarte,
                    {
                        projection: projection,
                        initExtent: bounds,
                        maxExtent: bounds,
                        minScale:2150000,
                        maxScale:100,
                        size: [600, 400]
                    }
                );
				
		carte.addContentManager('layersTree');

		carte.addAction({
			type: Descartes.Action.InkMapParamsManager,
			div: 'impressionPdf'
		});

        // Affichage de la carte
        carte.show();
}
">
    <input type="hidden" name="css" value="
html {
    border: solid 1px grey;
    margin: 5px;
}

body {
    background-color:white;
}


div#map {
    border: solid 1px grey;
    width: 100%;
}

">
</form>
