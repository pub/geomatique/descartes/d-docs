# Les services Web
---------------

## 1 - Présentation
---------------

Descartes offre une partie serveur permettant de solutionner certaines problématiques techniques liées notamment à l'exploitation de ressources tierces de provenance diverse dans une application WEB. Une application métier en ayant l'usage pourra soit déployer en propre la partie serveur de Descartes soit utiliser les services clé en main mis à disposition sur l'Infrastructure Cloud Eco du Ministère et maintenus par le Département Socle de la Direction du NUMerique du MTE

!!! warning "[beta] Services Web - En cours de qualification"
	Ces services sont actuellement **disponibles 24/24 7/7**.

	Accès au suivi de la supervision (intranet): [http://psin.supervision.e2.rie.gouv.fr/portails/MonApplication.php?application=DESCARTES](http://psin.supervision.e2.rie.gouv.fr/portails/MonApplication.php?application=DESCARTES)
	<br>Accès aux remontées d'alertes de supervision sur Tchap (sur invitation): #Descartes_Alertes_Supervision
	
	Ils sont en cours de qualification dans l'infrastructure cloud eco du Ministère : tests de performance sur plusieurs noeuds avec un répartiteur de charge.
	<br>Ils peuvent être utilisés mais nous ne pouvons pour l'instant pas garantir la qualité de service (performance et disponibilité).
	
	Si vous rencontrez des problèmes de performance, vous avez la possibilité de palier à ce problème en déployant vos propres composants côté serveur.
	<br>Vous pouvez récupérer les applications Springboot ou les images docker de ces services.


La partie serveur de Descartes est composé de 2 modules :

* d-wsmap contenant l'ensemble des services web utilisés par la librairie Descartes

* (optionnel) d-jobrunmap permettant de gérer des tâches d'exploitation (ex: suppression automatique des contextes de carte partagés) 
	
## 2 - Déployer ses propres services web
----------------------------------------------------

!!! warning "Information"
	Depuis la version 6.0, les services centralisés n'ont plus besoin d'être intégrés dans les applications métiers.
	
	Des services web équivalents ont été déployés en production et sont automatiquement utilisés par la librairie Descartes.
	
	Si vous rencontrez des problèmes de performance avec ces services web, vous avez la possibilité de palier à ce problème en déployant vos propres composants côté serveur.
	Vous pouvez récupérer les applications Springboot ou les images docker de ces services.
	
	Ce chapitre présente comment déployer ces propres services web

### 2.1 - Déployer les applications Springboot Descartes (WAR/JAR)

#### 2.1.1 - Déployer l'application "d-wsmap"

* Télécharger l'application "services-X.X.X-boot.war" sur Gitlab 

[https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages/?orderBy=created_at&sort=desc&search%5B%5D=services](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages/?orderBy=created_at&sort=desc&search%5B%5D=services)

* Créer les répertoires utilisés par l'application avec les droits lecture/écriture

Exemple: 
```
> mkdir ./data
> mkdir ./data/contextdir
> mkdir ./data/logosdir
> mkdir ./data/logs
```

* Créer un fichier .env pour renseigner les variables d'environnement

Exemple :
```
export SERVER_ID=localhostid
export SERVER_HOST=localhost
export SERVER_PORT=8080
export MANAGEMENT_SERVER_PORT=8080
export LOG_ROOT=./data/logs
export LOG_LEVEL=error

export APP_DESCARTES_GLOBAL_PROXY_HOST=pfrie-std.proxy.e2.rie.gouv.fr
export APP_DESCARTES_GLOBAL_PROXY_PORT=8080
export APP_DESCARTES_GLOBAL_PROXY_EXCEPTIONS=
export APP_DESCARTES_CONTEXTMANAGER_CONTEXTDIR=./data/contextdir
export APP_DESCARTES_EXPORTPDF_REPERTOIRELOGOS=./data/logosdir

```

* lancer la commande suivante pour prendre en compte le fichier .env

```
> source .env
```

* lancer l'application avec la commande suivante:

```
> java -jar services-X.X.X-boot.war
```

??? note "Utilisation d'un fichier 'cacerts' pour la vérification des certificats"
	```
	> java -Djavax.net.ssl.trustStore=chemin_cacerts/truststore-current.ks -Djavax.net.ssl.trustStorePassword=xxx -jar services-X.X.X-boot.war
	```


* urls d'accès:

Accès à la page swagger des services web: http://localhost:8080/services/api/docs.html

Accès au suivi des métriques: http://localhost:8080/services/manage

* configurer la librairie Descartes pour utiliser ces services web

```
Descartes.FEATUREINFO_SERVER = 'http://localhost:8080/services/api/v1/getFeatureInfo';
Descartes.FEATURE_SERVER = 'http://localhost:8080/services/api/v1/getFeature';
Descartes.EXPORT_PNG_SERVER = 'http://localhost:8080/services/api/v1/exportPNG';
Descartes.EXPORT_PDF_SERVER = 'http://localhost:8080/services/api/v1/exportPDF';
Descartes.EXPORT_CSV_SERVER = 'http://localhost:8080/services/api/v1/csvExport';
Descartes.CONTEXT_MANAGER_SERVER = 'http://localhost:8080/services/api/v1/contextManager';
Descartes.PROXY_SERVER = 'http://localhost:8080/services/api/v1/proxy?';
```

#### 2.1.2 - (optionnel) Déployer l'application "d-jobrunmap"

* Télécharger l'application "jobs-X.X.X-boot.jar" sur Gitlab 

[https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages/?orderBy=created_at&sort=desc&search%5B%5D=jobs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages/?orderBy=created_at&sort=desc&search%5B%5D=jobs)

* Vérifier la pésence et les droits lecture/écriture des répertoires utilisés par l'application 

Exemple: 
```
./data/contextdir
./data/logs
```

* Créer un fichier .env pour renseigner les variables d'environnement

Exemple :
```
export SERVER_HOST=localhost
export SERVER_PORT=8081
export MANAGEMENT_SERVER_PORT=8081
export ORG_JOBRUNR_DASHBOARD_PORT=8082
export LOG_ROOT=./data/logs
export SPRING_DATASOURCE_URL=jdbc:h2:file:./data/jobrun/h2db

export APP_CONTEXT_DIR=./data/contextdir
export APP_CONTEXT_PURGEDDIR=./data/contextdir/purgeddir
export APP_SECURITY_USER_NAME=admin
export APP_SECURITY_USER_PASSWORD=XXXXX
```

* lancer la commande suivante pour prendre en compte le fichier .env

```
> source .env
```

* lancer l'application avec la commande suivante:

```
> java -jar jobs-X.X.X-boot.jar
```

* Urls d'accès:

Accès à la page swagger des tâches d'exploitation: http://localhost:8081/jobs/api/docs.html

Accès au suivi des métriques: http://localhost:8081/jobs/manage/

Accès au tableau de suivi des tâches d'exploitation: http://localhost:8082/dashboard

### 2.2 - Déployer les images Docker des applications Descartes

#### 2.2.1 - Déployer l'image docker "docker-d-wsmap"

* Créer un fichier .env pour renseigner les variables d'environnement

Exemple :
```
APP_DESCARTES_GLOBAL_PROXY_HOST=pfrie-std.proxy.e2.rie.gouv.fr
APP_DESCARTES_GLOBAL_PROXY_PORT=8080
APP_DESCARTES_GLOBAL_PROXY_EXCEPTIONS=

```

* commande docker:

```
> docker run --name docker-d-wsmap --env-file .env -p 8080:8080 -v d-data:/opt/data registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/webservices/d-wsmap/docker-d-wsmap:X.X.X
```

* urls d'accès:

Accès à la page swagger des services web: http://localhost:8080/services/api/docs.html

Accès au suivi des métriques: http://localhost:8080/services/manage

* configurer la librairie Descartes pour utiliser ces services web

```
Descartes.FEATUREINFO_SERVER = 'http://localhost:8080/services/api/v1/getFeatureInfo';
Descartes.FEATURE_SERVER = 'http://localhost:8080/services/api/v1/getFeature';
Descartes.EXPORT_PNG_SERVER = 'http://localhost:8080/services/api/v1/exportPNG';
Descartes.EXPORT_PDF_SERVER = 'http://localhost:8080/services/api/v1/exportPDF';
Descartes.EXPORT_CSV_SERVER = 'http://localhost:8080/services/api/v1/csvExport';
Descartes.CONTEXT_MANAGER_SERVER = 'http://localhost:8080/services/api/v1/contextManager';
Descartes.PROXY_SERVER = 'http://localhost:8080/services/api/v1/proxy?';
```

#### 2.2.2 - (optionnel) Déployer l'image docker "docker-d-jobrunmap"

* Créer un fichier .env pour renseigner les variables d'environnement

Exemple :
```
APP_SECURITY_USER_NAME=admin
APP_SECURITY_USER_PASSWORD=XXXXX
```

* commande docker:

```
> docker run --name docker-d-jobrunmap --env-file .env -p 8081:8081 -p 8082:8082 -v d-data:/opt/data registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/webservices/d-jobrunmap/docker-d-jobrunmap:X.X.X
```

* Urls d'accès:

Accès à la page swagger des tâches d'exploitation: http://localhost:8081/jobs/api/docs.html

Accès au suivi des métriques: http://localhost:8081/jobs/manage/

Accès au tableau de suivi des tâches d'exploitation: http://localhost:8082/dashboard


## 3 - Utilisation des services centralisés (indépendamment de la librairie Descartes)
----------------------------------------------------

!!! warning "Information"
	Depuis la version 6.0, les services centralisés n'ont plus besoin d'être intégrés dans les applications métiers.
	
	Des services web équivalents ont été déployés en production et sont automatiquement utilisés par la librairie Descartes.
	
	Ce chapitre donne des informations sur le fonctionnement en dehors d'une utilisation avec la librairie Descartes


### 3.1 - Protocole de communication avec le visualiseur
----------------------------------------------------

L’appel à un service doit être effectué selon une méthode HTTP précise et comporter les paramètres adéquats. Toutefois, le visualiseur OGC, c'est-à-dire la bibliothèque JavaScript des composants, gère lui-même les appels aux services centralisés.

#### 3.1.1 - Services d'interrogation des données

Il existe deux services d'interrogation des données :

* un service d'accès aux données sémantiques de serveurs WMS : il est utilisé par les fonctionnalités  « sélection ponctuelle » et « info-bulle », et consiste à fournir de manière « unifiée » un flux correspondant à des réponses à plusieurs requêtes WMS/GetFeatureInfo.
    
* un service d'accès aux données sémantiques de serveurs WFS : il est utilisé par les fonctionnalités « sélections surfaciques » et « sélection par requêtes attributaires », et consiste à fournir de manière « unifiée » un flux correspondant à des réponses à plusieurs requêtes WFS/GetFeature.
    

Ces services doivent être invoqués en méthode POST.

##### 3.1.1.1 - Contenu obligatoire

Le contenu du POST doit comprendre un paramètre infos contenant une instance du schéma XML suivant :

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaoAAADaCAYAAAD32gaDAABNXUlEQVR4nO2dBXhURxeGv/XdZCMQLBAiEBI0aHANwd0KtFCclhYoVqQUd+cHipVCKdDiheJSKO5a3F1DAyTE1v57brIhCUnYGJE9bx+62bk2c+fu+ebMnDsjNwmAYRiGYTIo8vTOAMMwDMMkBgsVwzAMk6FhoWIYxiJolCA0NDS9s5GqaDQaSCSS9M4G8xFYqBiGsYjg4GCULFkSer0+vbOSKsjlcly4cAF2dnbpnRXmI7BQMQxjEeRRRURE4MCBA1AqlYgbh0WeiUqlyhQeSlBQEKpXr/5BGZiMCQsVwzAWQyJE3WV169YVjX1MyDM5fvw4tFptOuXOcjKLoDKRsFAxDJMkbGxssGPHDhiNxljpZPxtbW3TKVfJg8oQsxxSqTQdc8MkBAsVwzBJ4t27d2jYsKE4ZmWGvKhjx45lKi/FYDDg9OnT0eIqk8lQtmxZ8ZPJWLBQMQyTJMiwb9269QNPhDwtEZNREAEjTBIZ5DJBuEwG6A3CPoIASDOYjtGYm0KhEP8mgeIxq4wJCxXDMEkiJCQETZs2jTVGReNTR44cgdZGhZDry9G17RxcbjQNR36sCsmRKagx9C6+WbUInbxtoMigvWvkDWYmj9CaYKFiGCZJkOe0ZcuWWN4HGfhIj0oPky4IL+7ew4Plu3D0+/IoHxqIR49fIkhnAh1hMhpgNB8qkQremCAQwrlED004T/R5STiEI6K/Cl6b1Cwk5LUZY1xfmvG8NSb1YKFiGCZJUDefq6tr/BujegPlMhOKuF3B3ycMKKMXhCRqsz7sLf6ZVA0D1gUj1KiEptwgrFvcDq6PVuPb9rPhVNMf23bsgM4ggcLnK3ztsRiLt4ZDL9Gg3MB1WNLJGzaSCLzdNx5VB6zHu1AdFBpbtJ55GD/62UPNFi1LwtXKMFYGeSzJGYuJG+WXGFKFDIX8K+P6/tMwlI16QVgfjtCj8zH4UitM+aMJnE/MQr3xW3HgXku01b/Fw/uPsTe0HLau6YzH63qh/eJZ2OO7AGvWueDBmj74atoKHG4zDJWerEWf/qtQuu+f+L6KHPoj09Cifx94rF+ALzNw1yKTfFioGMaKoEi9atWqiZ9JHY+hKLmYkX6JIlXC1rsd6pwcgc0PtNCRxsmENN+uWNVnLcZ17oBzAU8Q/FaJq3cNMOaDGNTgX8Mfhb0d4Fq6GJTGG3AuVBEFPdVADjXC/7uAa/fCkPvoJmy/+wT6sW1wzFYC07uXeCac5+JdPQyFwEKVBWGhYhgrgryi58+fi2NMSX0xl0SKgigsQwK52gF+FR3Ra8Ut6PQyGPVhuLlhOLrPe43GY5ah36PZaDR6t7Dt/VH2NhrxUy5XQyYrAS93FaQSsweogy7ChIiQMJg0vui1YB4+L6QRhEkCqUwKhzxaqDiyPEvCQsUwVgaFYXt5eSV5jjuK8kvKC7ESuRy2lWoiV7+NkBh8YDTocOvcbtx+0ACFK3lCuz5Q2Ct2F2Tsb2pERY5HIxXO6VqxBopKF+Hacw0K1HHF43XfoctiN4zdOQg1JCqwVmU9WKgYxgpJzhhVko6hcTCJHHL7WvDz12HjDhNkCiV8W3aG96+/YJDfMagqFkcNwUN6+PAxDPmNkcfQoeb/x7peVPSfVAmHkt2wcOxV1BzfBKWnyyEJz4OWc4aipFoJ7vXLmrBQMQyTekgV0Hh9iUWHGkKZ1w62tnZoOuM4yo5UI6e7I7JJBmLVyS8QbBCEyVH4PigY+pxuyG7rIhzTWDhGC7VKApXfcBw6FIEcbhqoVIDXl0twuAGEc9gI321RpON0HKjxKupFYi2cPYVzyDk+PavCQsUwTCoigVTtBDdvp+gUO+fCKOxs/pYd7kWyx3OcJtYxEjtneHu/36p2ckNhpxi7a51RpKgzGOuAhYphGIbJ0LBQMQzDMBkaFiqGYRgmQ8NCxTCMxdBLwrSEe2YnK5TBmuDaYhjGYl6/fo3KlStHr9mUWWcc1+v1ePPmTXpng7EQFiqGYSyC1qHau3evuIaTKWq280uXLiVpDsCMBE3ZpNFo0jsbjAWwUDEMYxHkRZUvXz5WmlKpFOcAzKxk5rxbEyxUDMMkCzLy5E2xsWfSGhYqhmGShbn7L7N2/cWFysFL0WdMWKgYhkkW1BXo4+OTZYw7BYWYg0SYjAULFcMwyYIMu4ODQ3png7ECWKgYhmGYDA0LFcMwDJOhYaFiGIZhMjQsVAzDMEyGhoWKYRiGydCwUDEMwzAZGhYqhmEsIjQ0FF999RUCAwPTOyuflGzZsmHRokU8L2A6wkLFMIxF0GS027Ztw6RJk0TjHXfWdHrxNzw8PMu8AEyQOA8bNkwsOwtV+sFCxTCMRZAw0QzqrVu3xsCBA0UjHhMy5EuWLMlSszuEhYVh/PjxLFLpDAsVwzAWQ94S/WvUqBF0Ol2sdPO8f1lJqMiTCgkJQZs2bcRlQciTnDNnDgvXJ4aFimGYJEGLDu7evTuWR0WGe8GCBVlKpMzQasD+/v6iGI8ePRrTp09nofrEsFAxDJMkyHDXqlUrlkdF3kZW86bM0JpbPXr0EP8mkcqMKxpndlioGIZJEuRRHT58+AOPqnnz5lDIJQh/vB9zJ23AlaAghNNSVUotstXti9mfFYFcJk2/jMfFZITh1XEs/scBXZsVgUoRf97Ik6IuQPPfUmkGKoOVwELFMEySMC/vYTbeBHkdojclGH/9qwvYtvw36NqNQcuiMhiubsSo6T/C1/t3fF5KgwT0IB14if0LxmHuq+/QvWUxi44gr/HGjRvQarXidxKtAgUKsHilMSxUDMMkCRIlep8qXoyCeElkUGvtUbJlX/RvqIL+VAjW1J+Ojaefok1xN8hub0C/ObvxNlQPucYedfvORpvCgqE3GfDy8DyM/+MigsNN8CjojZcBL1Cp0xg0yX4Cy+btQq4vxqJ5YeDaxhFYebYMOoxrjRIqwYxdW/fRc74NNUBpmw3+30xB0wLPsH/mQIyZvxd3shvx9WQT5g2sA40qcZNIHtXTp09hY2MjfqduUHd3dxaqNIaFimGsDBpjSU4wAB2TlPEZiUQK2luii8A7wRNRyqXQPd2Pn0dOx+VcX6BZGQlM1zdj+si5yD6zBypLjmLBuNnYrm6JrpWdEHh4Ohb+HYzQKkNRx/0cNi1eDK9Kw9GwAHBn3yIsWtMWlYc3gdd/B7E4gXNWMhzAnDFzsCd7e/SolA1X1w/FuPl+KDu5LLK7F0K+bGpcdnJDUY8ckEktKxvdA7Mw0SePWaU9LFQMY2XQ2FL37t3FAIikQMETcd+dSgh9eCj2zuqOruuVUASchaLhQFEoAo6vxOxtF6CtWQpXjVIY7j7B6T1TsLVLO+R/8ztWnXZA0zXDMMTfAbft9mLp8fNQyCSQSGVQqFSQR4mJVKGCSqWAxBCBx4dXxHvObT06oqx3BAIePEJQ2B3ce+qC4t0WolIeN2TT5IbbZ1+i5qqlOOLVHL0/KweljAUno8JCxTBWhnlcRa1WJ+k4evnV0i4uibCfQ243uKiPY9mOGyj2vw6o5S7F5a138UpRECWLuiJ/DgUUXsWxqI0SBT2UCPrrLgLkzijgYguDwQCXAj6Qyv6NfV7BeyEHxuzEmARPLfB+/OcsVFAL23y10G/OTyh+8R8sHToNu+p/h5ZlnfCsjjdsBE9PbzTBZNAhXGeASsbmMKPCNcMwVoZK8ExoWiAaa0oKFDzxyy+/WLSvTPB4yrUfhuHlLiPXyy6Yt20NjjTugwLexeEi2Y/8/n3wQ30HhJxdgRl/2aOymxz2RYvCWb8Pl2+9haywE65eOgKjQS+cTRATowE6vQkBb4IET9CE1y8ojYbDZMhVOP5zVnExwvj8DPafeALb+oPwvwWVcerUKgyffgduXWvDNRe9vCwIn1wJtZJNYUaGa4dhrAwKCCDvKKlCRcdYNI+fUY/w0FBhfx2MjqXQdkBLrK61EGP+9MemVh0xvO0FDB3/NcI3OiD4ygUEVRyGVhIlclX4El/V3oOpU76DaYcjnIIBnYHcJznkuSqicrEgLFvYH30OuSDw9hsEh0bAKFMid8VO8Z/TGHls0OmlGLv7Glr55oQsMAxuTfxQQq2EXJId2ezf4vXf8zFothJTe9dgwcqgcK0wDJN6SGRQ5quJXhM0yFZCBYVciuy+nTFmanZc8HSAJpcnPh82Bs/XHkdQqBGKQjXRoGcTFNVIYFL5ouOP46DbdROh4XoUUpWGbPu9yHPmroSuoybB4WQwQvV5ULHKZFR+XUIQHBWUDuUTOWcV9Bg/BTLhnMFhBkjztMeY+p1QKY8g0sbcqNZzIiaeC0VEDlsOisjAsFAxDJN6CKKiyOmLNr19YyR6oHaf71Db/NW9NgYMrv3hoVIpspVqi4Gl6JsJIZt7onuEICIGk9jF51G7HwZ9eFgSzhkHqRKedfphQJ0klI9JF1ioGIbJeBgNkBVpiTFjiqBIcXUGekmYSQ9YqBiGyXhI5VB5NcCAAQ3SOydMBoCFimEYhsnQsFAxDGMxFHCQ1PevMjNUVg6ySH9YqBiGsQgKTX/9+jVGjBghGnB6HyuzLD1PYpOc/NL+VGY6hgUr/WChYhjGIsjQ9+/fXzTcb968EV/+7datm5ie0SHBWbRoEfr27ZvkY6nMVMaYs8UznxYWKoZhLIK8qDFjxoh/k9HesWMHFi5cmM65shw3NzfkyJEjehHEpMIzpKcfLFQMwyQZmpyWPKuRI0dGj1llZEMeOUegBF9//TUeP34Me3v7eD0kmq0jIc+JZuYIDg5O66wy8cBCxTBMkqGusF69euH58+fid1pQ8NmzZxl+vKpFixY4duwYdu/ejUaNGn2wfdu2bahbt26CM8s3btw4ybPOMymHhYphmCRDXtTkyZNjpZ0+fVpcpj6jQ+NV1AXo7OyM1q1bi54WCez69evh4OCAnj17JjruRscznxYWKoZhUgwty0EGPDMIFUFiNGfOHPz+++9o1qwZNm/eLHYJmoMtWIwyFixUDMOkGPJIqPuP/mUGyIvq3bs3Zs+ejbVr14rdlv369RPLkZTuSypvRu/uzAqwUDEMk2IokIK60jKLUJmZOnUqfvrpJ/EzOe9JUbkzchBJVoGFimGYFEPG2tPTM72zkSyoC5DJ2LBQMQyTBXiOs+seIFfzMsirkIF9nKwFCxXDMJkYGlN6iIOL/odJQ8PwTd3iyOOgYaHKYrBQMQyTSTHBqA/E5R1/Yc/e83gqL/qRcSZhf8MTnPpjH26YcqB0o+II2XkAV8L0kCqLoHZ7X+SVScEz+mU8WKgYhsm0GA0heHpFi+ZLByG8+N+JGjSTUYfXV7Zi3Zp9eKDOgftvd2HvrFNwLJILClxCaMlC6FIsG5RSlqqMBgsVwzCZFAnkKhfUHdIZEW93YPVHosSNeiPuHX2JWstWo0G2J1jRvjbkA//Ahl6lIQvYhvEb7sHo7QAoZZ8m+4zFsFAxDJPJ0cOyoHgJQqV2yKk2wvg2CG+N2VHELYe4xSR4WFppKHf7ZVBYqBiGsQpkUsDNJQS//bIKl8P24I+7jrDbuQK/B7pBbvoPsvwtKc4+vbPJxAMLFcMwmR6T4FMZdAbh/ybxm0H3DJd2vUCuesWQEy9xWfg7d72iyOXnj2y/TMAmvQJ+o+ahm+FnDPxtPUyFu2DO2DxQytmnyoiwUDEMk8mRQCbPgxINSyCPXCZ8I9F6hBN/nENpPy84wvx3EeS08UWv9ZvQK/rYSdjYIh2zzlgECxXDMJkcGZQ2ZdF1adn3KYIg9VzlG/Ut5t9MZoSFimEYhsnQsFAxDMMwGRoWKoZhGCZDw0LFMAzDZGhYqBiGYZgMDQsVwzAMk6FhoWIYhmEyNCxUDMMwTIaGhYphGIbJ0LBQMQzDMBkaFiqGYRgmQ8NCxTAMw2RoWKgYhmGYDA0LFcMwScJgMODKlSswGo2QSqUoWrQoZDJZlk1n0h8WKoZhkkRoaCh69OiB4OBgaLVa7Nu3DzY2NggPD0e3bt1ipdMn7Z+Z05n0h4WKYRiLIE/j9u3bcHd3x/Lly0UPhLh79674Sd7HiRMnIJHEXiWXjP3Jkyc/OF9mSWfSHxYqhmEsgjwNPz8/UYw6dOggfo+Jra0t/vnnH/ZCmFSHhYphGIsxmUxiN9/vv/8uelhmaEynYMGC4mdmh8p179495M6dO1Z51Gr1B94i82lgoWIYxiLIaHt4eCAsLAzt2rWL5VGRF3XgwIEs4U1RuapVq4YlS5aIokzI5XJUrFiRgyvSiY8KFbUuHjx4EN0fndmhB83V1TXVWn7UwqTWV8zWZWpDeaVxgbRszWW1eia4rlMXEqGDBw+K92Ht2rUfeFRk1E0mI0JePUDAOy1y5neCRiqBBCaYjKF49fAlgm1zwNXJBtIUl88EY8RbPH8SCEVOVzjZSJGat4zKRuVkMgYfFSpqXZQtWxYODg4Z8seTFOjBe/PmjTj4a29vnyrnu3r1qti6fPfuXSrkMH6o73/16tUoUqRImtVBVqpngus6baA8UflbtWqFoKCg6HQ7OzscPnwYarkJRybXQscVNTDn3wVomVMDhUSPsIAtGFS+Lw50XIGL42vDTp1Cz8SoR/idjRjeeRHcxu/F0FpaqFLJ2SHRzZ8/f5boxswqWNT1Ry2lM2fOpMoPPj15+/YtfHx8UuVcZLiuX78uhrMeOnQoTbs8SETq16+PZcuWwdvbO80MWFapZ8Jc16lxr7JiXScH8jKePn0KZ2dn8TmJi9gtpg9DRf/mUC9fhX2HQ9G0qSBUJj2C9u/EdqMaX9SuALnuDR6+CIKRHBaJFHY5XZBNI5RT8LoCnwRAp9bA9C4EERItcrg4Qv7uJZ4Hhon7S4T9tbS/Wg51gZaYuMZP8KhsoJQKXltoIB69DBb2M8U4r3CNiCC8fPEGJuG8RkFkI4RtUm0OuAgb47u9VL8kuhQBmJV6GDIzFo9R0UOY2ftnUyv/ZLhu3LiBLl26YMeOHaIXkpbQ+ek6DRo0wK+//govL680M2BZoZ4JKgPV08OHD0XDQ9/JwCa1lZyV6zqpkCdVtWpVXLhwIeHGjFwOZaW6aK39Hedv3EaEwQEKQYAO79sGk+Zz1CkThEcbR6H2mEOQmXSCt2VAtVEHMOUzF9jq9mBMuW44U7sZHE8ew31NC0zb0xOqxV+i3aJ7sFEAhvAg+E+/hP+1yg7pjV/Rrf5UeC64gkm1dHi+YTAqj9gPlWDVFNAL592LiS3doL23DF/6j8Wbhm2Q+8jfOPfuNYIb/A+XZrdBHjsF4ru79LxklPvOWGkwRUREhPjPjFKptPhYMly3bt1Cp06dRIPi6OiYBjn8ELrO9u3bRQO2YsUKeHp68g/pI5BhbdiwYfTfd+7cEbuoLCWr1jWV68mTJ6L3lidPHlG8yVt69uxZ9KwM8aXT/jF/N/Ejh1xVGf4tNVi1dx9u9iqOYriDm7dVcPisJkrcXI6uQ7ei1ti9GFZNAd2hSag9pCeK+qxHdw8TdDojrgX4YN/pOSiskSLi1q/osOgyvLv8hp/auuDfZR0wbed2PG7aFvlMRhj0ehgNEQi6tAw9hu5DhVF/YfkX7jAcGoky3WpijN15jPMwQic4RvfsG2HriQl49FsH+E/6DWv7NUOvYgrBG4tdArPnmJZjkUzSyLJCRT/G58+fx3rYqE+dfmi7du2KjuahllPp0qWhUCjEcNTEDAKdk4wdvUOyc+dOZMuWLc3LERO6HhlM6hpauXKlOJ5hCWRwPla2zExCda3RaLB161bxe926dUXjTEJlyf3IynVNol2vXj3RKzp27JjocYaEhIhpdN/oHsWXTtC2jz1HcrkKlWu3hHrVXvx9vSvc8A/23SyHflMq4cXBeTj5NhiyUY3xj1ICickAW+E6r14aYXITCwClVwG4qZRCA1JQELdqaFApG6b+1hl+c/VoPOMg1tZ2hotaAXNtGwWxun/kH1wyNcKC9oVgay8IYNWaaGT6A3fuP4TRzQiZcN7i7q5iiLm7e0FIcSuy6zEeSJCrVKmChQsXRtsJJn1JJaEywSS45C/fhEHtkBN2Kkm87nTkriboQwLxKigCJokSdk7ZYCNPZP9kQj+skiVLQqVSRXf3kPEhYRo2bFiM7JgQEBAg/ijprfvExmfonGTw6IXHT224zNB1t23bJs5DRj+6xLqyyHC/fPnSorJZiskUjqCXbxCmdkBOO1WqRloll4TqmmjUqFH0fnXq1LH4fmTluqZ9zGOR5t4Ec5qZhNJjbksQuRyqKrXRQvU7du/+F76yXbhWuiWmFRCel+Mawfj7ovf69ehbWmg0hAXiv2A5sufVQvoOoqCU8MgPuVhWCRRKN7ScvRv1jecxz28A1g2rhc1ojaVXJ6FWjEvKhIamDBEICxdcJ5MCgmsmfBO8JYUs2rZECqzEogabTjieo/4yDqkjVHrBeO3+EWV6bUTLBVcwtYE91Amc2agLwoUFn6H+jBtwdKyCoRt/wZfeNlCkQYAN/aA+FhxAg+7UtfLxLo1IaL+kdBWmBXR9MsqpXbaPo0d40G78WKYXNrZcgCtTG8A+oYr+xCS1rskQfYysXNd03rhlS6isSb8H1P1XBf4t5Vi+YxGWSP+FT/uZKKBRQ1KjAarYT8a+/ZfQLo8rHq35Dl//UQr/OzIElaOOjhYSox6ht/5An+YT8F+npfhp6w50OTwZ/hMC8PypCWYHWipXwL1GPVSaMwW//HIGFT93g/HwHuy1r4zva7hDboxpXD4uPnT9HDlycNRfBiL1rAyFi4aHQy/40yZ9KF6/fkdPuOBpCWnUitHYIbuNBEEB57Fr179A/Yn4a3RzeLioIdcFI+BtaNQjJIHGwQm2ysj3L/Shb/A6WBe5TXiANHbCtsQ8tjjQj5z+JbY9s5I+ZTMK7ZJwhOvj6b8XWqAR717hTWhUTSps4WivRMTb1wiR0N9qsdX77lUQwlR2cNIKBlD3Dq/efFj3woMj7qePeoYMErXgfWuhSqQ1bOn9oEiuc+fOoVatWpkmcCQjPMfkYfz333/Inj27Bd1/alT1b4dsmzbgqLQSBlcvIJgDNZTenbBo1h1UHNAVtRaZoFSXQf+136O6rRp4p4JWEAhHG3nk71uqgLpAOwzrux0tZ3RF7YVGwXNSocLA9fi8oPAs3bWBo7C/Vq2CbfR5u6DGQj0UKjVazTyGTt62kNy2gYOwH+i8Qr4lKq0gRI5iT058mLs+6RnhqL+MQeo3h406hFz9DZ0aT0BQ/eaw378b58NCoWv2P5wZWxjb+3fFnGOvECgfheYhRvy5sBnsdo1CpcE7xP5qhVyKepMOYnzT/HCUBOLM3PZoMfeaYOAEU2bSofHsfzG9eU7YZoyGfIKYf9T0ST8O+nETlqRlzrEkE3TBz7Hxh8r4cZtOaLQYEFFsAP5a4Y8zvVpgcmgfbFr7NbwNezCsTB/saLsYZ38sh6AdI1B52C6YDHrIZRKh7o9iUotcUIXvwcjSPXGudmPYHDyIu5pWmH18BPzs1Kny0KbmQLm11DV1h1aqVAmnT5/+eDeyIFT29Sfh0pVxwhepIBwqyMlBkdoie6NJuFhnXNSOMbZp62Cs4Dka5YIXKY/0ZqRCg6bEV6txqWuMl4sFsRK3F+6G5Wc6id8VwrOjinVeIQuCMFI67bfy7JdA1HGmOmMFD9UobI+6bjzwdEkZi7Qx9ybBZQ/T4bqmPi4cHY2HK79Egxm/YuM3q9Hxp98RGNgY0wvOxIEJ9aC5+wvaDdqOkt9vwPIO7jAcHIXyfaoJz/lpjC6wEZPnnkehbiuxoK0Lzi1ogYkbN+FBw87wFtx9SxzzmEYkLvQgmlvU5v0S6xKiH2rc89D3wMDAD9JpQLZChQrRg9M01kFYkhb3PR3KJ41XxP3hpHXZkoQ+HGEnFmHKyz74eVt95Do6FbWG78HhBx1Ro0YxGCfuxIFbXeBquInrBimaVC2D8Bu/oeegTfAbsw/DayigOzABft/3ROEiv6OzmwFhIeG49LQodp2cCi8bG2g0SiTm/1h6P2Lum1D3Tlat65TWMy2FYSkSmUKoM4XF6cIGKOJ5EVgqV0ETn6WSyqGK0fWc4HlpP03s/RJ739hczxz1l3FIM79EjLIp4A6tvR08CxaC1HgbepMEcsElV9C0KsLDR8/Yg8P7cF7XEHM7FIWjkxK6mrXRULcet+8/hbR6RdQurcbkxR3gN1+GptP3Y4ufM/IJD52lvcdkRGgQPe5MzwQZiT179oh/m98R+diAddxZCei8dFzc85sHt+mT5kajKCLCkrS4eaB8klGLG1qd1mVLEjLBmFT4Cutkv2FAk8Y4/+YF3oYqcOepAl3a90Drmd/gxvU3uPFoH66X6Y/JVRR48vtOHPnvDUw/1MFe6u4zRMAktLgfPzXClB9iBJjMswAKCmXRWjAWZun9MO9L9yQhw5tV6zol9UyCSKHzWd3TIDGnef3mzp3LUX8ZhDTtQJNKIiN3EjOQYp8xzdtFrTxq6AmfJqHdLJMKrSuNB75YfBhNwo5jRrV+WD+wFjYrP8OSK1PQwMGyLqCPrTFj7oOm8F/a72MD1tQituT89LAnt5Ud37s+8YUnp3XZkoJRF4Iry7oLjQmgz+p9mH1nDMr3/0uoTrlQj9Xh11yCHruXIff9C/BpPgmegvA8kMihti2P3uv/QC8fLeS6dwgOl8Ehlxay0MjGjk8B16gIsI9j6f2IuW9CY1RZta5TUs+Uh+PHj1uF8aawfI76yzh8gpGehCtbKrjg7tX9UVY1E0uXd0a1L9xhPLQPuzTl0a9qXujv/I5eracguPuvmLfvAL45NAm1Rj/Eo8dGGOn3bYH9IiFMbMob+uHG3C+xfePrCkjo/GQwLl++HD0eYd7HkjRLW6xpXbbEMEWE4O0bwRsKl4tBLjAZ8ejuRYSF+CNXbiOeHH8EmdEknleuUKOaX1MYO83BbFk5jJpRAGq1Sqj7evDVTMeh4/fRpaArHq3uja6/FsPMI0NQJeo6SYm8svR+xNw3IaHKqnVN17bkXaiE8mDp+1yZGSpncu8RkzakmlBJ5Gpo7bRQ0ztRUiVsqKWolFK8n7jNzo7m45KIc3ApbexgJ7SoJTIlbIt0wZKZt1B+UDuUn22AQiFH4+nH0aVYNtga22BQjz/RYvbnqDbbJAibDKX7rUf7AuoP3ibPaJgf9rhYmpZhIS9IJdTd5gGotCvy8ZGpbFHx+zWY3a4vyqydjWH++yGr64fGmusIePYEJqkn1NVqo7l0DTaVboBaBVWQy5VQxKj7inNMQt37oPcf/VHNRg1piHAd4b7YiM9QxiYz1bW5azE58xWSyFHXY1yhzWrpJMbkOVKjgqP+MgapI1RyFbR1J+D8uVGCCNlDLe2Olec6AGotNKQodSOjbNRaEqsi6L7iDDpIbaAlgyexQ65mU8VoHdHTFh4UFZ1DQRLngNLfrsWlbvrobXLhnLYqy1WKHsDEWuUxH9DMxqcvmxwqbV1MuHwNI2N2i5jrRV4Ya6/0gJ620Xs6U3UwCukqhCAwTA+Z0GDxbeSHgqqoMUZl7LqXkAhqbaGSCcZDuI4YAWZ+hizA0vuRGUmtuk5IVC3NQ9yuS+oGjK/rkz4zc7o1eI6ZiVTyqARRUWjg4KCJ+i4Il0OM9zrEbeYvMqhsHaBK8NjY55WptHBI5isi9OAVK1bso1Pl0A/Q0h8v/TCp/zq9ugYov3R96upJ7bJZgkSigEaozPhqi+pWG6eyaOzq+vIeaDH5ON7Jm2N2x0IxhCfhujdfx1KSUteWjrFkxbqm/SiAhKaXSmqZyAM5evRotCDSOShUnSIMZ86cGZ1+6dKlaE8lM6ezN5VxyOBvI6UM+iHFHJcg6EdKb9rH/JHSfpYYLzICe/fuRZMmTcSpbXLmzPlJDRj9kChqrHr16vFO8RJfi9vSsqUVUkGIvDosxtGWekF9hAaMo02aPHQJ1TWlm/82z/JgycuxWbWuzfPYHTlyJMkNGHPUnxky5JQ3+ozZlRg3aCUzpzMZgywrVPQjpJZRzB+4+YXF2bNnx5qUtly5cuJS0x/74dIP1cXFBRs2bEDjxo1FA0ZTrXwKA2aek5Dmnxs/frz4QmJMzOWILzggJd09KUfwitX2yK7++J7JJaG6pjBtWleIiFnvdD8+ZtCzal0n9C5YcsmMXeZM5iPLChX9gJycnGKlUYuaWqIUumvugyaBIgNk6VQ6dF5a/ZOW4qYJT2k5Bjo+rXn16pVouGhC3Vy5cn1gIJJajqxEYnVtTo9b75YY2Kxa13GFLyVQ3qxhTjwW5PTFYqGiefzoX2aG8p8aLUl6aN3c3LBmzRpxvSNajiEtV301z+Q9ZMiQBJdwSK0fUlaoZyK1ypDV6to8j11qeNgklL6+vpk2QCUpxJ3ZhPm0WCRUNCNz2bJlo1tOmXVqEfpBkQFLDaNO53B3d8fq1avFcQTzej1pAbWAx4wZIy5mF1/eaS0tWlMrpT8kcz3TNWjsgc6b3i1JqrPk5IWe0dSaNT4r1TWdMzW9QsoPw6Q1HxUqannR0tNmcaKBRlpyILMOOJLYmgfYUwr96D08PMSFGM+ePWvR0hHJvU5CE5hSNxAZrpSWKWY9kyGmmcV3796d7u/9mD0MGiMyT+xqKVTX5vDjlJKV6tos/AyTWfioUNEPhlp3ZkigHjx4AL1en6YZyyyYB91pLISWBUgrAxYfZLjKlCmTKsIbs54peo5a7BTplhqLLaYEav3TCrsdO3YUo/CS2u1Gz2lq1UlWqGsS7dq1a+Pvv/9O90YIw1hKlg2m+NSQASlVqpRowD6FiKdW6zohKJybDGPcFXPTA7r206dPxVV8zas0x4UaUIlNh0TvI6UWmbmuzffSGsaVmKxDsoUqvccuMiIU8kwGJa0XXCODTNdJq/ejyGuhMsTs7qXurvTs7qXnjcTm+++/x5QpU2LNHECzgVPwwbRp0xI05iS4qXm/MnNdc1AAk9lIslDRQ04D7lmJ1PzhkkH5FPcnNUOM40JGncKyzZAhplZ4eo9LknDOmDFDFKSpU6eKYkUiRe8aUbqrq2uiodKp7UVkxrqmRsihQ4fSNHKRYVKbZHlUaWkkswJZ9f6kd3cReVUkRgMHDsTgwYPF6LhRo0aJ3yndPB3OpySz1XXcRkhKoHttnnYoZg8Lp3O6JelJgceomEyFOVR8wIABGDlypChS9J27oi0nJRMVm4+lqZgoVJ8+yTs7ePCg+MnpnB5fOgXxpMSbZ6FiPgq1wimwIiNRvnx58Qfg7OycbKNrDTMqxIUMB82yQeH+STUYMY+lbk96CZq6Y2OO/3E6p8eXTpHENG9mct/BZaFiPop5puyMRsGCBdM7C5kOMhS3b99OlsEwH0tGp2bNmrGWtCdPi4Tvn3/+QaFChT44lowXp1tvOnlUKYkFYKFi0hEDIt4dxf+aHEeFv/qiilYFjkfL+FCjZdWqVR8E16R2ZCWTdaDAp3379iU7iIeFikknSKQOYFKNPlh2qwa8hNY6v9mT9pChSK7BMB9LLeNOnTqJXYFxt1ljdyrzcei58PT0xPnz56MbOPQc+fj4WORpsVAx6YARutD7+HPI3/CY1hPVO9yHItEF5w0If3cQ02oPxRZjYfRY1A6Pvh6Jze/0kGlbYdrf36O6LXtjlkAGw8vLK0XHUhfg0qVLY3lUZGwyWwQk8+mh7mLzS/L0IrulUbosVEw6IIVclRe1B3wFU45LuCh9mKhMGXU6PN1+ENof5mB2jgAcmTUMv3l0wm99K0IZcAI7tj9FpeaukCm4Nf8poFlLevTo8YFHRfMgam1UCL21Bt91noNTwaHQC3ZIprJB2e9WYF47T2iSWkdGHcIebsEPf+bBhF4VoFGlZnPEBH34bazp0wGzTr5DuIFWHy+P/ivnom1BFeTpHUia3LILx4l10ONnXKs/Bju+Kw/TyTloOvYBui6cgbaeGnzqnwo9K82bNxdf2Dc3aJISrs5CxaQLEqkaOQrkQ9jbyx/d12iS4MGbnKj1eRkUlYUgwCEnOrXrgAqVckASaoPTvz+AxOQCEkAmcahF27p1a6xfvz7W7B5JOZaiuObNmxcrIOP9ZM8GGENf4O71Wyj0w5/oV1ULlWAV7fLlh0qe1PoRhER3H+vHjcWftuMxLlWFQ/Dqw+5gbb8OmHCrMkbOawMPmQF3143A6M97w7ByDtqng0F/TwrKLuSZ6uDmmbM4e3cHTvQqhVKvH+Hff2/jRWj6dLHTs0KLmyZ30gAWKiYdsewnIxX2y2Z8jqsBeniGb8PqnddwwekKBpevDMWra3hmLCrswSJlCWQoLl68mCyDYT6WoGmd4sVooKYy5Aol8haugIoV1O9rxiC09I/ORL0h6/E2RAeFjT1aT9mFAZU1kBpCcXRmXQzeEIRQkwb2vr2xaGIpnPy+A0avu4SHmkHwC36Bn7+TYdGgVXAb/id6+wKn5jTDuL3+GLG5L3xxCvOajcbNSlUQtvNv3FE1wYTtfVHuwk9oGPeaFaQIP7YKU3bI0Wh+PzSu4gqtIAalPBej8CMj8rgIHpVJyO+h+POrQGRZ6gvb3kRv240BlWTQ312HwV8tglPlqti2a7fgVSqg8emKLi4/Y8mOcOhldijf71fMalUAaqk+nnuyE31KP8KGfm0x0lz2dwH4ZbILtrWbgFti+fbgmrEgvIo9wGP7b7Fkegu4GE5gdrNx2FNrMP6oa4RcsO7eOS/i7zNGFDfSApfkwQjVEBGKQ7Nj3uu+WDqjGfI+3oQhKcp31L1JwPFLyfJQLFRMhkcqPPludT2xtGVNTIsIgLr+OAw0zYVf9aGQeH2G8eMaQsHdfhZD40k0PpBUkjKmoAsNxuq+FfDPjyrYaFtg4vY+8H3+F4YMWop87X7G5Apy6E/MRY9B38Nl+QQ0fLoIA49VwoDpDZH79AK0/t9mHOlbB7W+HowWl7/Fr/m7Y8q3NeFs+BM3z50DAg0grQ28dxbnznoKX4XvCMTd0+ewVVIL86fPQF5NDrg+3YSh8Vwz39IxKHX5LJ6FFUX5irmgjvJYVDkLoVRO+kuPsFvr8H18+V0xCU2xBUMG/IzQyqMxq4ULTKeEbQMawTR1PXo6PMPls+dxu0BH/DJlMp5uHoxeq6diS+9RmDLVGY82D8cPk1ahbcMB8H0R3z0ZjHy/jUaVmGXvLZRddi5G+WYit0yJZ9u+QdeVG3GgdwO0xmUcPXcWTu0LQiK9BJlchsJ1SuPivgswlYxqmBjCEXJsSZx7vQkHvmuAVmEpzbdwb1ZOw2cFP/REyXunRUfJK0/O7DEsVEy6YoIRBr0BkR0SFDRxHAvanILvup4ojXNYLP79Ncq7NkHfnwqhrUkKB/ficDP5omiDIMDODT75tZCl93hCJuL169eoXLkyHBwcsH379ug5E2kFY/PMAvGlU9fe2rVrLeoylCnVqNZzOr6prIVGmRuegqV5eGA91v97F6b/+uPW71IYA+/hzkPgxO3xaF6hNeb02oafR/yAf5/dQWigDJfvq9C2agm4OSqhyOMN36LOkNyQit2M0qj6lkhlkMqkUWOcgscgVyAiX2FUr1QRTvII3Fg2NN5rHr85HIVCwoQj1FApKbjnJtb074qfzglpGns0HbEaDW/Hn99jV39E8WebsO2eL4avaYrqBbTQez1E+an9cfTyQ3SrCCiUKlSu3gDVquRA8KuSUK/ahOze9VGxki1uXbRH6MpTuHQ/DDmOx3+Nk7cnoFn12GWXhV2ANLp8leCkNOGNc3c0+mUKrt+KwIMXh3GpWA8srp8LqgAJVQLsin6OaienYMdjFXT0E5MqYOvz4b2+dEeP5nkkKc73iZsT0crjQ6GixhFNGnD06NFkrTjAQsWkIzR4XQX9thaHgxi1J4FUXRTNRueFo1oFFcx/KyGXquFRoSI8oo8thgrO6Zj1TAqJDK3rRasfx4zUo8/JkyeLRoQ8p4TSKwkG0hJIQJyLVkbVqrZQCDbTFPEOj1+/QYS6DDqOEITJXQ2lQgG1SoZs+RR48vdcjF34GKW7/IiWT5egw4wDCAsXmjEGI4zUAjcJDRqjKTqyM3IgXuxljAWJVlFvD6iFQ4xCAyg0gWtmd7GDTaiLcO5HePpcApl7XtT8ZgKcz6/DsO9W4e4rPd4Fxn+sY14bvF35GqHSosiXWyU2tkz2jsgtCOjV0HDhe+Q0Uzkd7UXvQaG0Fe51MXh7qIXvQqNMKAckYQgPMyIkofy520CBD8ses3zUKW6TtzHq1fkREw5ugvudI3DxX4UyTnLIXol3CXKbXKhfToH+fz4ELZ9m0kfg7h7hXi+Ke6/f39eU5dsWygS6/lIyzRkLFZOOCMIkc4RbWcf3KbJs8CifLepbzL+Z1IDEiaafii89PhFKKN0STMbI7jmFnOpVDpdylVAIK/FS6oXq1fLj2eah6PWLMwYsd0fQ0bU4dqE+ujapAe+NC0QBivSaTJEjmVI5FIKRNglGW28w4cGz58I+Jjy9byQ7Hg0dotWoxD+kiVxz8OqiKF29LTp4CF7U1C2ot+AzuPtUg+Hf+XguEc4pCG1Cx37/uxe8fcvDQ7ce67Z+h3pt8gPnT2OXoSBalcsHuSy+NdwEz1IVU1iFZ1+eSP7WFEMeU+yyS+KUTxQieXb41a2Jb0dOxayw3Gj/XVHYCvuafRaJXAaHmtVhN2QYJIaiQn1E4PqRhO41UiHfJeAq+VCpyDP//PPP8e2330KlUiX5WWKhYhgrggwGvay7fPnyZEX9WXQsiYnQfBecoffhMjIlspXphjlDLqPJ6Gao+D8VjK8VqDT8J3hpbGCo3woFFm/Aj01qQ13MGRVCInD/4TPhwNxwzhWMNxuGoH7EMCwYUR/Nq4/EqEmt4PeLF+z1QQiJ0CNSqwTPQ7iuzmD66DULKlXQOFdB/3kToR8wHK2qzIBCMNDvXtzCixrD8E1le+TRxn+sp8oGTlHnbTy8JSrPUEAW8hyFB69FtzLZobgbs/wmMcCEVoM2Z8tk1IvejVGiQPaE8qdQQqZ3fl923X+YN0olnie6fCAhksPJT/Cq+u7GLs928C9qKwilBHpzHZgE78qpNmpVeos/9+qEBoMSPnSvl8S9109gyKdPhXwr4g1rokCcY8eO4auvvmKhYhgmcchg0DhBcqP+Pnqs0PpXF2yLmVsrQeNlg/ev/ggtcTsPlO85DRsqPkS4zgiZIge8y5dAXrVg/GoMxdLdrfBKZ4AkjzPydh6EcDcXQRDVqDtiF/Z008NgVwD5c+VF+6m74PMoQjDYdnBw7IOwsDwobGMDG1RD/61bEepaEOqoUPiErulM/ZGwhatvGwyb54brAZEGWSJXQuVSAj7OKsGLSOBYpXCsMnLbRnGbQSi2Eq4lBS/LXrhPnjHKr5RCUa0/tm4NhWtBNZRKoGDb2dhWEXDzFLbbJpw/k8wpRtk94JbNBgPjlA+CaChz5YSLSoHi9eqgmFYQJsE9el8HWtjaaNF0+m54PdfCzdsBzkXiu9fucHRsJxxTJcX5jg/y0jjqj2EYiyCDQUKTnMgry46VQqZ1gU9ll3i3SrJ5oloNzw+PssmLktXyxnuMU6GqqBZzvlPPqqj+4SkEcqBQ5RwWXzMSDfKWrIH4r5z4sQlti1t+aY5CiJktrYsPqsS8PQmch8b54pbdvnLO6L+NujDc3TgYPecfwM131TCkkw/so2LD4+bBybMKqkRfIqF7bS8c836tsuTmOz7IA9+4cWOy14tjoWIYK4IMRnJe9k3psUzqI5EpkKtCBwzN1gRShStKFXJAkt+p/kRQIA4ty3PkyBGO+mMYJnHIYFSrVu2TH8ukPuRx2bmXRx339M5J2sNCxTAMw6QpISEh6N69Ozp37szBFAzDJA4ZjG+++Qbz589P8tpR5mN/+eWXFC2Cx1gf1N23Z88edOjQgYWKYZjEIYNBs5wnZ5yAQpX/+OMPBAQEiDNUkNCFhYXhiy++EEPXaeyKFlSkl4Q5ndNjpqfkZV+ChYphrIiURP2R4dmwYYMocgqFQgw3vnHjhjhBrXnmimvXronbSNQ4ndNjpg8dOjTZa5axUDGMFUFeEL2wm5wl48nwNG7cOPo7GaM3b96gVq1a0Wk0j6AZTuf0mOkVK1ZMlidPsFAxjBVBrdsGDRqkyrmoO4cmiI1pfGJ28XA6pyeUnlRYqBiGSRbmbsSUzDjAWC9J6YJmoWIYK4KWkR88eDCmTp0atSJv8qHIvyJFiiR7tgHGuiFvy9LoURYqhrEiaHkPitgbP358ioVKXBIiZ86P78gwKYSFimGsDO6qYzIbLFQMY0VQtN9PP/2UrKg/hkkvWKgYxoqgqL/PPvssvbPBMEmChYphGIbJ0LBQMYwVQVPcjB07FiNHjkz2LAEM86lhoWIYKyI8PBxLlizBkCFDWKiYTAMLFcNYGanx3pP5fSz6pDB383tZnJ426eQJOzo6Yty4cVbZwGChYhgrgozf5MmTk/UOFRlLOnbYsGHi8cWLFxc9NFq2wXw+eoGzQIEC4vtaSqVSnGKJoE9OT37627dvMWvWLPz4448sVAzDZG3ICHbr1i1Zx5Io0TpWvXr1wpQpU0TPjF76pU9ap4oM6MSJE9G/f/8PjiUx4/Tkp9O9p9cKrBUWKoZhLIZEiVr5Hh4eYus/ZrpZuJjUh+4rdfvRUivWCAsVw1gR1H1HXUjUSk9qFxLtP2bMGLGVT4sn0rnibmPSBvKEv/rqK9y/f1+czJWgBoOrq2t0N2FWhoWKYawI6kIioTJ31SUFEig6jjwpBweHWMfTNvN4FZN2PHjwIHq5DBoPdHFxYaFiGCbrkdKoPzo+JCQklkdlbuUDRujDr2PHzE24VLw5BjTwhkr+qQxp1LWnrcOpCCOU5dthcH0vKCRGvDy1Egt23INEURZtBtWBR+BJrFi0DznaDEXjImooEsmi0WjA3Z0T8OuhIETItCjStDe+LJddEIgkdHOaDIh4fgJ//HoQTs36o763Ckm5LeYGhq+vr7iAJUFCZS1drSxUDGNFkBc0fPjwZEWOmQf0KRiDWvXvxSlyUbxIATRCF3YVf02ciNXtvPGNf6FPKFRSGMOvYcv4sfhLmxMBV9zxpV8BOEte4Nivo7FoYwRevG2M4r1rwU2qhI2dHdRyCSSJZc+oR/iVvzB+wm946NUQ+e78jt9PSJF/2WDUzKeCzFKdEIRK9+wIlk+aBC+vXqhdKGlCRY0CCllfuXJltFBZE9ZXYoaxYqhrrl+/fsk6lozlJMHQdu/eHaNHj05gL73QypdBIQihWkEt/thbyTu5t2sSfjvyDhESDey866NLy3y4vW4VjilqodNnpeBovIu9//sLFws3Qj/BI1I+3ItJKw4hKMwAmdoO1ToOQ10PEwwRN7Fn7ia88PRE6OmLeCYvgabfAApBgLwbNEDBsw/wQBCaHLiH84ecUb++En/uIHEhcZJDpSZPShCQp8exeuU/0BcUznP2PB7LhXxVaoeB/h6Qm3SIuLYTG28UweTf5uCLt5Uwf3sIlLLIaMfXZ1Zg7pbrCDWpYFelA4bWKyAUUvDAYpWxAbp/Vhwa4ZpKuqZwrISCT+7sjqdckg/uGcNCxTCMhdC7UuZw9GQhiEbYlS0Y/fNh2BQqAe2dLRi39hm8KvRF+L4FGHniLgpVnwI/01EsGzMSd4bWwFelBBGZOB5/vauCmm6CDF4TvJuJ+WA3/jP4qC/jz1GjsbdaD7QrogFsDYI/JxMcKxmk3r6odfYEHr3WoQQe4a6kNip4H8Ffu4R8mPSIeLwf838YD6/cn6GU+yEsFs5zv8VgfJszCA/O/Yb1e96hYvGRqJpHBplnIRQL+RubZo1HUIuuGDo8r3ASgyBg2zBxwhKccSiJwsaLWL7tOrIpJ6J99jNxyvgcRWtMRE3zbSPv6uVxrIm3XJ+jQm7FB54aecD04q+1jgGyUDGMFUHdd/PmzRNfIKVIskGDBkUHQkyfPj06ICK+dKJ3794peuFUrlDAu0YL2IW8RKBGAcXzAzjzcBx6fNkW5bZsx7mbevgEncN1j7r4rrEnAk8PxYQ/TiN7Cz/Y2sqgl+px7NeR+PPzpihaWga52haBtmXQe2Zn5BWMe8TbzZCK/7nC1Wcx7t03CJJyDxd8XNEGxxDd2xbTu6G/bbSwoWuOroDA1QHY+d0JnHppQCVnJdTFmmLogJdYumEEhhy8iHftm6FJ5yZwOrICPx90wdxLM/G56ix8521HGCSQyeOWcT9O3tKjmmPkpU2GCDw79msC5WqJMjkFoYqz8C3VxdChQ3HkyJHoYAprgoWKYawI6r6jbrsvvvhCjNwze0f0SeJF20mIEkqnCW2Ti3iud+Fw/O9fnLprgibwHYymxwh8K4OT/xdoUWIJtpw5g+J39sNQYwLqFpTi5Z5reCrLBxdpAJ6/lEORty5GjlajZG6l6HVIZFK4FPWAfYRBsOYmRC4JSWKRHT5lHTHqzF3UwX44lh2D7Pr1SKhXTSp4im45HaCP0MHW3gkS+SvIRFWTCuLqjabjpqJ4BTWK7LuIv8aNw3PPcugU8B+U8uLIZm+AybYCOo+sABqje3XuVJwyPsGrQCFvDlH3wWBEwO2EyyXnrr8PYKFiGCuDuvDmzJkjelRmSIRotom4JJT+cQRrKxO8EfV7EyMxhePfNUMxZEEZLL61CvX2dcKa448hlQqekdQTfs288OO66VC9CkP1aaXhJHhfRo9CcJa9RtkuUzCtlha6G9vx635b+HjLIAuJFJj8ObJHRb+975KUyeQoUNYPDxftwz48hN9XBSA/LYsnn+8xGt+Lc3SaPhw3/l6AnXdKouW3YzGp+no8W9EDV68HwD67FuG6i7j7WAZJofv456fNeOzTCDn/GoIhi8vGKqNMKokWSRLX7ImUK75oc3ol4Oeff0aRIkXEaD9rg4WKYawI6kKi7rvkLEdPxnL58uXo1KlTLJGLi4nGYEKC8PboCowfcwZqhRRSuRL5KreCVz535Nbfx/afxuJoUABcg0PxIiBQUJbc8PJrheIjRmBfrs/xR9kcUCgVcK7SAX3rncesaT/A5p/seH1yFy7m+RZlvyyPXHSd8HBBLAyxry2kRQhaI3HJh6InF2IdiuLrcYJMnIpAeLgOBjE4MXI/nfDFJPwdQV2bgqcDCnIw6CL3M0aeL+jFfizp8xuOP2sMz9encdBe8J7K54VLzrZoV34ofhO8zGced7Br2RPUnV8HjVw94i2jMZ9evKYeiZXLBLd4rDJNTjtCuDcU9WeNs1OwUDGMFUEeEq1FlRzIWP7www/iCsEJC5UUclUh1O3TB/bBwXj+5GFkqkIFVWkpWrUehUGPN+BywCugfEcM/a4sAj1kUAgCKi9eHFVsNHjt1xC+OeSCFyJ4Knlq4JsxP+DBwn149uw5lEU6YPzwL1BabYJeUgh1hOuUKpdLHGsyX1tM83WGTV412re6hhOoAN+8Nsjm2wZ9+uREIbWQl1zl0EbYL6eXDTTZy6Ft374AnUfw4mSF6gj7lRK+KoRyylG6w1SMvroI+1++wAuZF1oOaoOv/d1gL3HHhBGBmLT2XwS8yoEaw79Bv0aecHw1Wijj+thlLKCAJvf7a9rkTahckgS7J63lnan4YKFiGMZiPm4spVCoS6DN5KloE+92N/SaUjVOmgEvz/yB5bv34GRYQTRpL3hLFNoeeUFIvVti6qyWcXMiXqfVuBKJpLmg1dSpaGXeXPNrjKsZ9bempuBlmb/URK/xNd+fpkQrxDqtzBstJ89E3BwQTlV6YXqVOIm5qghljJsYWfavx9V6/zXecsUPecI0hVJinmxWhoWKYawI6r5bvXo12rVrl2SjR8ayZ8+eqR4ibdLrEfbqEe7cU6NMz+/RtUaeKA+JMWOemZ6j/hiGyfJQ993AgQPRrFmzJAsVGcvx48enep4kchXy1/0e8+um+qmZLAILFcNYGdYwiWlWw+wJ58+f3yrrj4WKYawI8qK+/PLLZI116HQ6bNy4EW3btk2DnDGJQZ4wTX3FUX8Mw2R56B2qadOmJetYmjG9c+fO4lpUPXr0EMWOxkvIeJqXUO/QoYM4aSqnp246eVHW+P6UGRYqhmEswuyNnTp1Cl27dhVfjH38+LHYJUUtfhLBGjVqwMbGRhQ1Tk+9dPru5+dnlTOnE9ZZaoaxUqj7buvWrWjcuLH4zlBSIMO5aNGi6O/U+qeF/EaNGhU9We29e/eit3N66qZT1x95WtYICxXDWBHUYqf3cW7evCnO9ZcSyJhSl1RM4xlzoJ/T0y7d2mChYhgrI7WixqjFT1MxJWc6Jibl0H1P6WrNmQUWKoaxImicqXnz5qkywwEJXs6cOVmo0gm6/9YSqs5CxTBWBI0zLVy4MFXORUaycOHCqXIuhkkMFiqGYRgmQ8NCxTBWBEXq7d+/H7Vq1bLaUGcm88FPKsNYEe/evRNX902NqD+G+VSwUDGMFUEh5TTDgTWvbcRkPlioGMaKoO6+2rVrc7cfk6ngp5VhrAiajofmkmOYzAQLFcMwFkGBGEePHhU/q1evHj2J6sGDB8VP+s7pnzbdWrCekjIMIxo6mlTW19c3yYaOAjHq1auHSpUqYceOHeLxBoNBnI2dttHyExUqVBDTw8PDMWnSJE5P43RrwXpKyjCMONdfixYtcOPGDdjb2yfpWDKMjRo1wooVK3Dy5MnoJdGHDx8eHaRBqwATZEz37NnzwTk4PXXTrQUWKoaxMpI77Q4Zy/Xr14ut+rFjx4qfMSGR2rJli1Uu7MekLSxUDGNFkNdTvnz5FC3CR4JE3VDU7RfzvOXKlUuNLDKJQPf83LlzKFiwYHQdkjer1Wqz9CsHLFQMY0WQt7Np06ZkHUtG8uLFi6KRHDx4MIKDg6O3kaFkbyrtIS+2YcOGWLZsWfS9JsGiccOsPGaVdUvGMEyqYjaSV65cwYwZMz7wqCLHp0wwGt7g4cW7+C+7B3zyO0Am/VQt/ahrX7iNl0YTpDk9UZKuLzEh9MVNXHkYBCERBUvmh4NMiqTmipbUePvwLG4+08EgUcDRrRi8cqqRNEdGyGPIC9y68RQq9xLI7yBDUm8PeU7mfwR15WZlb4pgoWIYK4LEhYSmaNGiyer+I6NIy6L36dPnA49q586dwqca4e/+wSS/rljTdinuz2oMe82nMjNGRNC1q7fDKqhhbLMY9+e3QDZpEM7O74Ams24ixNgOq5/MQmN7TdKMn8kI3Yvr+PX7hpj9b06ogp9C2+ZXrBvZAO4OcstFz6hD6M2V6O0/CYWW3MH0xvZIyu2hOitVqlSKum4zIyxUDGNFmEPMr127luSoPzKOPj4+4hyBhw8fTmAvvWC0JZDKZZCTqxDHgpNXEvToAm6/NAiyIuxjnw9e7rZ4e+8WXkicUcjDCUoE4fHlBwh0yI9iLg6QBj/GxdsvoBe8JIlUjlwFfZBPyLrJ8BZPrtxHqFAOw6tAhEoc4OxuglylQsmyVfD45gPcM+pha7yDY3+/RKWKxXH4jELMn+gdxc2HR3aYAm7j5vMwOAnekrMyEPduPoExdyEUzA6EHZ2HcfvLYsSe1ah/eRh6/bwNh5/7w8VOBuN/t3H14RsYTMK5chWAD2UQH5bV211Il0ghlUV6UkJOYHobX/kk8Yofdfft2rULR44ciY66tAZYqBjGyiDBSU7kHxlJen8q2ZBX8vImlo9qg5/PCR7P21e4l6sntq5rgauDGmL4y57YsmUgShl3YVSNr/FP1zU4N8wbD/4YgqY/XYODwgCZ7g0Kf7sV0zt7I5thPybV6ITjtVvD7eZFPNE0wsgNRWGUKyCv6odmv9zB41A9vPEYdx82h1+38zhxwZyPG3Hy8RV2bu6L7DvGoM2wbSgz+Rwm512Ijp03o+DEzVjc1hUSQRCzGd/g0c1rCKkxFfs+pzEiE/SvbmP79B4YuCUAakMQXvr0x57pXVBY9eSDa+zaOgAlohflNcH47gkurfg+nvIVQ15t/N2T1rKqb0xYqBjGyqCW+IULF0SPytwFaO4SpNV6ScTiS6fvxYoVS/54iFEH3bXN+FPeH9MXloX2xP/QdNw+HH34LRo3qw6nwXtx+FYfeOqu47Z9XnSsVwYhl+eh16jdqDJqKwZUVkB3ZAYajvwaJSpsw7eegj8ikeNucBn8fnwRvNUShL3dih2QCnl1RYFS23HvgXBN3MO1UgVQU/YvpAnm428cfjAAfdqMwo8HLmLYljVYjZV4W3cSRrQtAJVa8IIq1kMnz91Y1qs6fqo5GfuGNYanlzNMf09Bz6X2GHPwL3SUbsag7ovw099+mJR/+wfXOHS7D4ooou6HIQKvLyxJoHy7MLCsFqo47Qmqj6tXr8YaH7QGWKgYxoogEXJzc0OPHj3EcaV9+/aJnzTu1K1bN3HcKaF0midw8eLF4hhJst7FkgqeTtFWmNVkFUZ064Fbbx7hXZgRj17I4Nq8Oz6b/AUuX36Oyw/24UGFAWjrK8PzpQdwMcyA23O64txCiXBdGTw8tTAGkWdB55RB610AecULmD0N2i8vPIs+wcbrb/AG/+JJ0ZbIK43yUIR8yOLJx4PHJsh83NG6T0csrDYKYySlBfFpDXelQhQ4lW1NDD98GFUn18TAtbPRosZafLVlLRo+eAipxAsu+RTQ2HXAooMdxLyEB9hgVmjcaxhhcovMpdGgx8PjCZcP8ThO1HXr7++PpUuXWlWEJQsVw1gRJD7Hjx+PN51mm0gsPSgoCEWKFBE9LIvGt8TItPeCZtKF4t8lnVB/uiPGHjiAVle+R7FvNoHMkFxRHrVaadFmx3xkf3gTvt0rw1Uhx0OtA+w1Pvh61VYMLW8Pk2Dw772QI4+nLeTBECMKi7nlixJOY/S1ZHIFylRtim827Udl7EXT5uOhuBIZgGCMCMGlXzqh8Qf5MMFkCMLTkxdx28kFuXWXcfHkEwSVKIjsEhOCn93E0yAH+P54FEe/3YTvCvfCkeMP0VgpE0TzNYJDhDNog/H85hOEaJ3wcnknNJwR9xqxbhCUiZRPmbz3srMkLFQMw1iEpWMjJtHgG6B/9QDXrl6BViUTBUtp74T/3ryEypgfhrfXcfjmfdjoDYiI0AnCYosKtVpAO2c+FmtqY1ZlVyjVCrhXbwp/57HYKQhOfbkLHq8fhqG7q2LugYGoSNcR8mQ0xbm2kGYShEvq6o4Sl3djN0qgRl/h+zXz/kYEvv0wH+HhIQi4twljRhxD9bGr0fHO5+g1fBTW+v8PnfOpcGJBK/SYVwKDdgyA77PTuCTPBjcnG7i6lIVLtl+wdf15FCx7BnNbz4Nh+G9o/+p5PGWNiMyf8E8iiKlbguUbglpaFeLG9pEgFypUiKP+GIZh4oOMJL3sm3i3H3Vf2SG3R37kPTcXHdrOjTxWaYPy/X/DzM+HwH/nLMzv3g3SBg3Q3uMNAt4FCC6QPZQVq6Gpain+rNgI1d0EL0umgLLQ55g36z5qDPwBX2w0QmlTHkNWf48aNkoYQ4TrCPkJza6Kehcp6tqU5qSBwsUTZcMFAUFjdHNRQCN4SQUL5oajWoPynYbBf8eMWPl4HvgE51cuw40CHTHhi8Io/3Q4/A/Mx9KVF9BxWFVUGzwbnbYNxPwvO0AvVcLGfxhGdigKJ7kX/hh2H+1mdMGXejlsag/B6i+LwuWRcPzfM+KU9RUkSke4CHnMba+CbYLl+1CkCPJw9+/fL3rF1jROxULFMIxFkJH8559/PhJMIYNKWxujT5/HqLibqCtQ8G5+PdslzgYTIt48xO17TxAkdUDV5tXhppBHBj7IVND6j8bp86NinCbq+sJ1Rhz1g0nyPpRbFSutmJAPfyEfksiXcjsvwdFOUX/bdxby0enD/JlOoPH4GPuc6BSVbzq5f5xyRe0HBbw7/4r3p4tK907gGsI9WHK0S3Q5VAmVLwGs4QXfuLBQMQxjMRYbSEn87wHFdw6jLgx3//wR7aefgyHvF5j1mQeUCmmixyR6nZhpsbZLYs0iEe8545zvg30SLFfsc38s33HTLb2vFH1579498dOaYKFiGMYiyDg+fvwY+fLlS/YM7PEhVWhQuPNynBOdD8HgSxMWOWuHoi9p0USKvuSoP4ZhmDiQkaxWrZo4MW1SZ7X4KDRbA6uTRVjT2JQZFiqGYSzGGo1kRoK6CPPmzZuqHm1mgIWKYRiLICPp7OwsflI34LNnz6JnsjCnU9j106dPOT2N0vPkySPO83f69GmrajSwUDEMYxF2dnaikVQoFGI3YN26dfHff/+J0YDHjh0Tt9NLwTVr1oye4YLTUz+dZgixNlioGIaxGBIpgozmqVOncOLECbFlf/ny5WgPYMGCBZEvtArfOT1t0jnqj2EYxgKUSqUoXGaDap65IuZKs5yedunWBAsVwzDJwlqNZkbDGuqBhYphmGRBnhR5VNYWgZbRoHn/svpMFSxUDMMkCzKQvr6+Wb41n9EhkcrqjQUWKoZhkk1WN5BMxuD/jj/jMOcjU+AAAAAASUVORK5CYII=)

où :

|     |     |
| --- | --- |
| Élément | Description |
| Layer | Liste des couches devant faire l’objet d’une requête |
| Name | Titre (alias) de la couche |
| Request | Base de requête WMS/GetFeatureInfo ou WFS/GetFeature |
| Version | Version du serveur |
| FeatureGeometryName | Nom de l’attribut contenant la géométrie |
| LayerMinScale | Échelle minimale d'affichage de la couche |
| LayerMaxScale | Échelle maximale d'affichage de la couche |

Les deux derniers éléments ne sont utilisés que pour le service d'accès aux données sémantiques de serveurs WFS.

Dans le cas du service d'accès aux données sémantiques de serveurs WMS, le contenu du POST doit comprendre de plus un paramètre pixelMask correspondant aux coordonnées pixels dans la carte, et formaté selon le patron suivant :

\#X=<abscisse>\#Y=<ordonnée>

##### 3.1.1.2 - Contenu optionnel

Dans le cas du service d'accès aux données sémantiques de serveurs WFS, deux paramètres sont nécessaires selon la fonctionnalité visée :

* gmlMask dans le cas des sélection surfaciques. Il contient un flux GML représentant la géométrie du polygone de sélection.
    
    Par exemple :
    
```
<wfs:FeatureCollection xmlns:wfs=\\"http://www.opengis.net/wfs\\">
	<gml:featureMember xmlns:gml=\\"http://www.opengis.net/gml\\">
		<feature:features xmlns:feature=\\"http://mapserver.gis.umn.edu/mapserver\\">
			<feature:geometry>
			<gml:Polygon>
				<gml:outerBoundaryIs>
					<gml:LinearRing>
						<gml:coordinates decimal=\\".\\" cs=\\",\\" ts=\\" \\">
						791779.9552533333,1854000.6277333333 ...
						</gml:coordinates>
					</gml:LinearRing>
				</gml:outerBoundaryIs>
			</gml:Polygon>
			</feature:geometry>
		</feature:features>
	</gml:featureMember>
</wfs:FeatureCollection>
```

* dataMask dans le cas des sélections par requêtes attributaires. Il contient un flux FEI représentant les conditions à respecter.
    
    Par exemple :
    
```
<PropertyIsEqualTo>
	<PropertyName>type</PropertyName>
	<Literal>chateau</Literal>
</PropertyIsEqualTo>
```

##### 3.1.1.3 - Format des flux produits

Trois formats sont disponibles pour les flux construits à partir des réponses délivrées par les serveurs WMS et WFS :

* JSON
    
* XML
    
* HTML
    

Le comportement par défaut des services consiste à fournir des flux XML. Il est toutefois possible de choisir un des deux autres formats en ajoutant le paramètre format dans le contenu du POST.

Notons cependant que la bibliothèque JavaScript inclut automatiquement ce paramètre lors des appels aux services d'interrogation, et avec la valeur correspondant au type souhaité pour l'affichage des résultats (cf. chapitre «[4.2.3 -](#__RefNumPara__33583328) [Choisir le type d'affichage des résultats de sélection](#__RefNumPara__33583328) »).

#### 3.1.2 - Services de génération de fichiers

Il existe deux services de génération de fichiers :

* un service de génération d'images PNG à partir de requêtes WMS/GetMap
    
* un service de génération de documents PDF à partir de requêtes WMS/GetMap et de requêtes accédant aux légendes
    

Ces services doivent être invoqués en méthode POST.

##### 3.1.2.1 - Contenu obligatoire

Pour les services d'exports PNG et PDF, le contenu du POST doit comprendre un paramètre paramsExport contenant une instance du schéma XML suivant :

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYIAAAIYCAMAAABXMRsDAAAAPFBMVEX///8AAADAwMDMzMxEREQzMzNmZmZVVVXd3d2ZmZmqqqoREREiIiK7u7vu7u53d3eIiIj/vr7+b3D/AAC0iOu/AAAUfElEQVR4nO2diYKrKBBFYbKZtZ/J///rqBSbgiiileWemdeddiGGo2CQAiEAAAAAAPrI2XAf8dcxO0ehoDRQwA4UsDNfwX8NaxzJzwIF7BgFdLvT/L/bj+4ABYVxrgJ9vzl+3wkFpekrMBfD4SDEUcrTYAcoKMxQQfdbyl1roKpkv1SCgsXEv+z6Csym/f2hYCky+mdAQWh/KFjKDAUneUZBtAJpBZerPKsLoKmOBzeoULCYuIKJ+0PBUqCAHShgBwrYgQJ2oICdvoLZQMFSAqf9f7PZ/rC/CShgBwrYgQJ20AOCHShgBwrYgQJ2oIAdKGAHCtiBAnYWN9OF+r+AOSxtrC6y628DBexAATsFFaDBLg8oYCemYP4NDhRkAgXsJBRUN3m9qAiPai+P7VIV6EGdfVXsh9oDCvJIKLhVQl5VhMdJ7u7NUgr0MAp2Zg8oyCP2FZdePf4O1L/dZnq3mfnLpgQFeaSuArkbKDDroaAICQVSVjqjm4KojTOgQI+rrO5QUIS4gq68uXRVcJfRTc38R9XxXrSBH39QUITpX82uZ/GQt5GUoCCP6Qrut+YOtBpJCQryQAMFO1DADhSwAwXsFHx2DAV5FOlZjT7WS4ACdqCAHShgB/0e2IECdqCAHShgBwrYgQJ2oIAdKGCnXDMdz/F/AcUaq6EgFyhgBwrYKacArXSZQAE7Yx0a+3c5o/N5QEEucxSM3nhCQS4JBTrEw3QyRYRHcRLdenWIh+nJjgiP4sS+41Ie2xAP86vbCuEF5UgURDbEw51MAhEeJUkoUCEeTjwHIjyKk1CgQjwonqOdzwMRHsXBVzN2oIAdKGAHCtiBAnaggJ1yz46hIJOyPau3P/4vAArYgQJ2oIAddHxgBwrYgQJ2oIAdKGAHCtiBAnaggJ0PUDC/xZD7iOfxAYc7+xA/4DO5fMDhQgE7UMDOfAWf1WIIBex8kgI35mHstgcKSgMF7Ay7Gh8OULApQwU7XAUrkwg7MRNaRPeHgqUkYx4EFKxMTMFJ7nYU5wYF6xJTUO2l3FdQsAFL+7lCwWJ+W8GCbr2xhvr5rfo/rqBkYokUo2ugoFhiqQRj66BgQWKD4mU8vchaKFiUmOwvmPPu4aXzq6TfVBCsZtOpTd3tmzt5RxX8u0l5Cs8kF7qZkYFXUxRM3O0XFVzkSdwj8ykmFNiPDwVTiCk4yLt6cZTyaFrIHnvZjlAk/9qBomh0qH46UDCTsVbJlqO8XBoHpOAkq+bKkPJeyaseHaqfTliBpMGM6Mdwi1nl11cR+8rqPiDsDwHlz/9N2+mURD+10W/H8d1+h1RBNKogmA6ugpnEFJypOqaCiIaA0gURlUrn6QVR8u2hYPDn5dA1zlN1TENAVVQdC2Hn/+7vCAUzKfbVzL6Cgnms8O14loK8L9VfRfE2Iu+ePNlGFLyFh4JFifWayBItpeFvUYub6T7r3nZlBePPCyJfZAsdExQkU5SxtoQfV7CMgIJ4ktHWnN9WEFwyZ/fA45LZ7WmlFHxIa91CBYOtQ597bosmFCwj9LlntilHFZxv8nYJfww5nGPkcxW0JMoOD+n8jOXrLANRBXd5FEf9HGO415QL8h1ZrqDLfc/A0s8db719iIc8dI+MbhXNMKKfIqm7Ae8p0u8oaPLfN7CWAt2fV6oe1mqGEdN468wxQpv/kIK+gcUKBre6erlwH17YGUacpc72n63grQgURMKfYcQV4+wHBaUYVMdSnndNka9mGPEKIv8p0ncr2NKc8xhJ3ZS21fG+ohlGzFMkZ44R2u+rFWzaCjn8Bj7t3b9agfQfzPtP5IsDBYndqBJc0cHvNVDM3I2yHwqywVXAzvI7ou3rgqn7/YCCdU9++14lG23fkA/4atYys8WkWFvJFkABO1DADhSw8yEKvhkoYAcK2IECdqCAHShgBwrYgQJ2Bj2rux8lf4IEReMLQA7Ic3ZWVoCyKM26OYT8n8C6dQEUTGCsF7MzGsvUrjuo3eezrgLxOQ/R+YhlrRnwRs2YYUeIakMoqn3XnZP+okG6qls3UogXZSGgIE1Swc69CiiE4iR37dA49BcN0rVvAy+kH2UhoCBN7NusO+yTVUAhFP6wXP1Ro9woC6GeOW78oT6LRF0wUDBYKQYKeslBQYKYgt6MGZSxFELRrDzbYbnMIF33e2isLihIEFPQmzHDiWhsstepeffmKvAX2hShIEHWnfv1LB6RYWSHbwAFCbIU3G/NzWp4MOXhG0BBgvXbiKAgwfptRFCQAArYWb1dDXVBikC7Wtknx1CQIqCgbHpQkKK0guHuUJAgmOFFyyIoSBA+57MjKlqmDNMFLCsoKD9M13ezhoLiw3R9N6soKD1M13eDPg7sQAE7hRSg42I+ZXJOFkvpB1lBwepDs3wZ5RVQNzw4mMo6Cool/AvgKmAHdQE7RW9KcfLngCxjBwrYgQJ2oIAdKGAHCtiBAnaggB0oYAcK2IECdqCAnWE3XAzAtTHIOXZWD/EAKcopQFmUSTEFyP9ciuUcFOQSy7nZQ0JBQS7FFAiElWUSqwvio3IdvVG5ju7OUJBDUkFvVK7Lpcl1MypX95fdGQpy6H3FHSoQroLh6E8YgGsxiboACtYnpiA8KpctiM79gggKcokpiI/KdfRG5UJ1vJisb8eRUbmgIIssBZFRuaAgi6JtRFCQQ9E2IijIAQrYKdm6hrogi35dgAG4NmfJY7LBAijIYUlBNNgXCnIYKsAAXBsTvgrmDXrz8QNwyUxmpRV/9+DSqQq+ZACuzNI4tNvcnI6umKzgOwbgKqdgLKXoc4Hg0ukKvmIArmIKxhOKrEXHB5GvoH/KpdIJr4cCkZMJwWo2nUzmboFkZu/z5jhPzEc/3c7MEGM3mqcgb7dAKt/mwOu0MLad3XC4L5OCLxl0xVdQyYM4yF07aeGt0k9n2w5VziXyNgqoB8XHO+gVRH9y3+S6VB0YqI+C7lDV3yOSl1L1LtE/pu42/XBdBTlpvBv9gugm5SPQYcdToGvV/hfg0W/H8d3mHu4XXwXdr2t7zicUDF+xXAXfWRc0Zc+x60J13smTLYjEHAWpN1qkQLeMfsHJb/DrgrYG2KvqeO9Ux83qy1Wee3sUUABEJD9Gr24oKEyGgoLfjoHYsI0o2JAJG6KcglRLabgtGQpEQQXjzwsizflQIEoqGHtq9slPVFYn+o02QfCcHtsYBsaY8aBw/Cnh3O2BppiC8ZQC74y6gCinYCyp0DtDAVFQQTyt4DtDwbshF/Ssjv0ErEABO1CQZu08QlmURI7+ucI7gD5Q8G70unPYfjPS/SX8pVPSA7OZlNnTFES/k4BRdKcY8di3fcraZ9nXi6CxP05C6OG6OuJjdUFBmth5bAqik6zkrXlxr+RVdCPgVFXXxcP2LsNYXYvofZnt9agx1YHt0CSdWsKpIWR4oCgBBUlid0QjCux6KFiBoQJdEOnsPclz5QzX1YKxulbBKKioOjZneFPb7s1VoMqu+FhdULApobG6oCBNwe9OGKsrjw0aKKBgHCh4N0orQF3ADhRkUPjpsYCCFMOSZ2FZNNgddUGC8vVvP0UoSBBSsLD8kb0lUJBDbtcyxZeM1cXLMgVfMlYXLwsVfMdYXVsRro6XKviKsbq2Al0c2IGCrwEdF7nRjRGADU/Bl4zPshWFcspVQD0o4GAiKykol/L3g6vgW0BdwI95QoOsBx8HTlp2oIAdKAAAAIC6gB0oYAcKAABvCIbe2pgSUTZQsAgoYGf9WDOwBeiyyA4UrMOhndxLY2d3DIAgjkVE64Kqudm82OVjlQQULCKq4E+e1Ohb3mgfZokalot2goIlRBVcrw95dTJc2PG4aMnObAsFS4h9z73Lv3a6U3+cJzMelzt4o4CCdTh1+f0XGGoLCjaiHfbpIg/iKqs7ZTiNx+Us0dtCwRJidYH8J8SjuS29XLuhtrrZHdV4XLQECkpRpI0ICpZQpI0ICtiBAnaggBvUBYtAdcxOX0HWADhQsITAaZ836sT2h/7FQAE7UMAOFGwLukywAwXsQAEAAADUBexAATuDQb4LjZoPwG+Dq2AJRXIPCmaxRpQNHuHMYpVAJyiYQzTP/92kPA3n6rM7yt4rJwoECkpwkSdx789YGcadflcBBSU4yLt6oeZtbSdev1UHWYlKHvSk6zTTupqU3ZmvGnXBEvqli569WMrdTu4v8k/8yYuedJ1mWreTsut9oWAO4xOve9EEzb/rTdyuZrppfx0UZBK7I9IFkZ/N/6S6JMSoAtQFs4jdEZ2pOjYF0XnXLHnINhJWK6CZ1qFgJS5Nrbuv3OpY/WUnXdczrZOCLgpEAQUroE/xvRNnGZxpvdsYChYQK5eUAilvF2dhZKZ1KJgH2ojYQRsRO6s0LEMBO1CwhCLPjqFgIXk9q9HTOpdiUTZQkAsUsAMFAAAA0O2KHShgBwoAAO9GmfgOxHrMYZVHNmAOqyqA0CmsmktQwA6e3fADBfNxuvW2g4aPbGi23O3dv3ygYALRMaulOI86sLuMlPhQMIERBaLtrKhm7rirDqaPvbxVQkd4qLAPM6eB7Xx61V3uUBdMYawHY/OPZu64ykclD+Ikq0aLjvBQYR92S+qC3cV7UCJQMIHY11nTY707y9uL4Gx7lopIeIH05pqAgmVQQaRzc/d3M39NV4C6IAO/Ov6nZ+5o4/ya4kUXRIJyuwv76BdEUDCTeHUsr+0NkZq543HqurFXVB0Loavjpo5uQzvc6hgKZrKgjSjdEIq6YAoLmnGg4AOAAn6gYD6Fm5ehYALrPjWDggkEFBR8dgwFE1itZzW6WC8ACtiBAnagYFvQyYEdKGAHCgAAAKAuYAcK2Ok30xUN70CQxxsABexAAfgA1o41Q0NdEihgZ6Q3nQ0vcCbouMmHM0SvHapavXI2pA2gIBsvxMO5tzzKi2j7jvZXOD1+vWSgIBvqWV3dzAQdKtijzf5WQ7eCzn0zfLi7ISUDBdlQ99xb16NaZWwX7NEWQm1hZFY0/+/bWA+pSyXakICCJGOzeDT/Hn8HEzSgVt/krq0KzAovtMDdUAEFSRKxZm2G+3EbR7lvqwKzIqDATRAKkoxGXP5rflWUsxTsIdqa4SLsCtnO5nG/WwV6Q5UMFGRDIR6XbtqsboIOFexBc6nQil517G2okoECbqCAHyhIggYKdqCAnZUVoC5gBwoyKPz0GApyKNyzGgoSrB9lAwUJoIAdKAAAAIDubuxAATtQAAB4N9YI8UDoxzhMeQEFFihgB3nxbmxmBM13MaCAHUfBoes111ttQjo0wTk87ILLVfajPtQWUGCInvaV6jva23qwefD2sh/5EdgCCgxRBX/y1EZqqKk67K+ux3s3eriN/qC4DjuHh5T0ysR8HM1MIPS2UGCIKrheH+1sHGaqDj1jR9eTuh1D30Z/yP4cHt1m+pVeRzOBEFBgiIV43OVfUxvsnNgBG0KgtnKjP4KbuQEH0swEQkBBDKPgJFXMZVyBF/0xQQHNBEJAQQxbj3bRHO3MTTRVh56xwxRENvpD9ufw8BXQOpoJhJKHAkOsLpD/umiOB03VYX51IR1ddWyjP9zqOKCA1tFMIJQ8FBhS38WodijdtAkFFiYFqAuioIGCHShgZysFqAssXI9soMAQaHfb5OkxFBi26FkdY/tP+ylAATtQwA4UbAt6ULADBexAAQAAANQF7EABO8O+iIjZACAPXAWlyM5JKMhGjv45Ix20u+VSSgGex2cTzfN/NylPVWDFYKYOBRSU5iJP4m574TpE7j+hoDQHee9+S3k46Ik5/Ck9jnql2g4KCjGIUJJyZybmcKf0oI7UO7M9FOQyNouH+U0Tc3hBHTbQQG0HBYUweWoLImF8+FN6+ApQFxTnTNVxl8s0MYcN6nAiOjRQUJyLCpBUuawm5vCn9DgKKHgrUBeUAg0U7EABO2gj+mCggB0oKEX2s2MoKAi6SrMDBexAATtQAAAAAAAAAAAAAAAYqevna1O4P/C7UT83FjBPQXdw9Zw9+unXtKB2VrRJtn+rg2l/0to6enzOijr9EWrzQZu3SmyeewU81UGtrmDKHv5nDCmobSZ3PJvjb/+9yIVeW8fezluRoWBklzo3GzdS8Bw//FCWRHLQ20pdAbXoPsjTUxBPQMxT4B1OfJdsA1sr0Geqc9Y+VUFCi9VSpyBpjrBuNn62J6Ha8aV2p4yhTHUSUflR62ypBZVSwq5wjsO5fMhnoKzSb+wcYq+oc0uh9kO1ByyGL9RO7YUrnl2+NP8/uw9JP4T6NOoguqX+ul5hN1OBOVWGP+1LdyllMhU1voKXTvepzyKTPfR+T+ftanP69lYEDqRfVj2fT5tOb3vvAzr50r1hd/S9F5SxL5XztCvpEE+TlvpI7lKz9SIF6rjF8KdQb2mKGb2O3rI2H39YH3TrulOl+89Wx21W17Xyp2uM/gpKyXvL9sXTK6u8w/YP0VL7Cp7CnuXeC1pf02fW+W9ed+tqujKfnTl33auIgqfziZ76k9SmHHe3oD3pv6ACcxWQJ0/OkxTUdCX4K0zN4r1ld8U83augrusJCtx8qQX9N3zxosuAznST/8/+kufrZa+FYgr0hWwV0Ou2pHQri56CLociCkxd0BWgXmVN5zddAi9zJesVroKXq4CORm8rhEgr8Irorhpq/w1fWEW+gq50pGw2rtZSILyTqqYcrN2CSG9hzq/uX0gBpWQrDNHP3G45FVfKmfbmFUTmLemCWaRAlz7P4QtarWthpyAyxdHL3bBgQUR7Rq4CdZ6b3Paq45pOY63AK27s94KuAHWvIuft6B4jeByBq4COxpwAvc3dA+99PK2AivPn8IWuqmt9zpvMF/rnS9+/9avjpQqe3VGb+zmdW4IKIl0gDW9K6X7u6W2pP7/9dkyHR3lGd5E1GahfthxSi8x71y//oNReta/A3JTa+1ExouBlatThC7qzdKvg3k0p3bGZ21GzbqkCFiibnHJoLV4lsLeik5s61v1UBbAKVjZQWMH0xqb/AR/CMonGBzwxAAAAAElFTkSuQmCC)

où :

|     |     |
| --- | --- |
| Élément | Description |
| Titre | Titre de la carte |
| Largeur | Largeur en pixel de la carte |
| Hauteur | Hauteur en pixel de la carte |
| Couche | Couche de la carte |
| Url | Requête WMS/GetMap d'accès à la couche |
| Opacite | Opacité de la couche |
| UrlLegende | Requêtes d'accès aux légendes |
| UrlLogo | Ressources HTTP des logos |
| FichierLogo | Ressources fichiers des logos |
| Auteur | Auteur de la carte |
| Copyright | Copyright de la carte |
| Description | Description de la carte |
| ApplicationsInfos | Informations sur la production de la carte |
| Creator | Nom de l'application productrice |

##### 3.1.2.2 - Contenu optionnel

Pour le service de génération de document PDF, le contenu du POST peut, pour préciser les options de mise en page, comporter un paramètre printerSetupParams contenant une instance du schéma XML suivant :

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAacAAAD+CAMAAACpxzohAAAAPFBMVEX///8AAADAwMDMzMxERERmZmYzMzMRERGIiIhVVVWqqqrd3d0iIiK7u7vu7u53d3eZmZn/vr7+b3D/AABCKHhMAAALqklEQVR4nO2diZajIBBFYbJ1WpOO5v//dWQVUNxFKnlvJmkjAkXdgGisA2MQBEEQBEHfJL5MR5v9dVrmcXBKLXCiIXCioYWc/jXa2BJoSOBEQ5bThGnc6WwPBqfEcjiNH9seDE6JFXK6cH4RH65X8cZvP3d++2VF2bw7HQ6cUisY9y7897cBxflJ7Hk85evGyoI17+hPCRW7s6C2xHvzkh/0tng9/65mrzoYnPYWj3wc5FTyEzil1TAnO+4xjxPnBTil1TAnO49gHqeGnXi/8Zc+GJz2VozTvELAaW+BEw2BEw2BEw2BEw2BEw2FnJYJnPZWTwf6t0zpbf8mgRMNgRMNgRMN4VEhGgInGgInGgInGgInGgInGgInGgInGtrmPmy/jmnRZ2qT3zUmFQ2tETjREDjR0J6ccHd2O4ETDUU5vUpe/pq9Ib42RE1sRWZ24LShYpwe/MIu/BHLNSWeDZw2VIzTlT/Zk19NiJp80vwusPyZELU2WE0nX0yyKQucttNgHIBkIEPUGg5Fwc8iWK3QIWptsJob0CaTdRHgtJ1i9xBaTuav7Tr6Txus1vmjiwCn7TQ+7rGWlxe41gargdP+GptHGE53/lLjnsOjaMk4AW3gtIei8/JfNS+3IWrNROHcBqrd+KsNVnPnEeC0j3CdS0PgREPgREPgREPgREPgREN7Ph8BTttpu3gNRHHsKXCiIXCiIXCiITwUREPgREPgREPgREPgREPgREPgREPgJLXpfU1fs6qLG7hb07fUNt4ZqmAXs2MlL8BBhNPeGZJyGqoslgZOyypYUfJwXZFUcFpWwfSSw7ucY1VFgl+2smdXzec08x7wLm7oPV2O17QwWw5Kx0l4pyzMttnbxnspFVfOr0UstVMom8dpWbYc5IUneMEKER+t4MROvOwkB24qRfRKGUvtFPrtnFjUR2s4qQfoVdiXE+9lI8BkiupMcpdMtfn+bHcEJxMPpwfxRzMKnQs/w8r+ZMK+nHgvGwHWpDS1Xwpnl2PY6cTPYaERh3OVybx1j/gATsZ9YseNPwt+9TOsPD+54UT2j+04zSnpcuM/QVBYN7DITAm863RTS/TyPJ4tP8VuL3TCrcSW6E6vIP+acc/86Y3raimEQWE9nDrFf1p/4pGPvZzY6a8Mzv07cHIiwMw8wtl148VDHvg6ibNY0IiPPT/FON3V+B/2Jxkq7GXYlpNckqyNALPzcrVLRoPdmlOmOnGev2geEeNUnOWUoeUkfPS8u1czKsNB17n+XLSv+O/gNDl/Fpw+/34EVU4TLBmrivc+9whOyypYYclwXbz/+VRwWlbBGkuGKuOR54jBaVkFqyyJ12YwgVPk+P3U6/Whg3tNJ8JpE++MaE24w4jm1jXqkFzUY9YG3hnWIgJLLZlrODitqGCNJTPtBqcVFayyZJ7Z4ERDdDh9tzJ1SKZmHadMHZKpWccpU4dkatZxytQhmZp1nDJ1SKZmHadMHZKpWccpU4dkatZxytQhq+/DHmP2fsq0Qat/19jKkFyUaYPAKVCmDQKnQJk2KPXvudkLnGiIGKepkzlwSqN4f5o25QanNBrj1C7fVvoPlpujwCmJRjjZZYyc6D0/AzglUfz2gl3oKIwK8/ODUxKN9CdwykRTxz0nes/PAE5JNH0eccY84kBNu86Nz9HBKY3AKRBpTgP5wSmJwCkQONEQONEQEU4p4tSyFg1OQojXyFDgFAicaAicaIgOp+9Wpg7J1KzjlKlDMjXrOGXqkEzNOk6ZOiRTs45Tpg7J1KzjlKlDMjXrOGXqkEzNOk6rb3jGn+CaUOreZq12TzZa+wPCSHkjhcZ/N59awmxjiCoBp6EyY2ngFGh/TsNFxp5QmVPG3MIpanNO4Q3QsRL70zfi9Dl3Yzfk1Hv+Hi9wSjZwGggIK1jRP2WKrY3VV+gEThOyuWaJhZ+0xGJqsUXn5NFfwenFXrOmtmk4MWfJk3gcgE7/Bk7lmZ3LxgtFqZYuu17FKiQXs0yMXNDMW8ksEaeniHiSz5e3w6V53PzKbz/3tsN9Bae/hoZYKqeUi/bI5d7u/PQwnOSCZt5KZmMO58uXpfLGvfLZxmuo/mQ/Pp7yZTrcJ3GKXME37eU/XFB5/l3Nelj+skvNy1vJbNNl3qJmsUIuH8gcK/o+qqM/h1Mg1yHKdazkpygnbyWzNP3JJQJOoq1ntXBZM/EznJpx7xX0J2cls2Tnp2t03PtKTj9yRUv2a+YOTEwp/jxO3kpmqeblZWEmDnrBuZedR3wfp17dXs23ubNYczfvfpzm6Vs5PcrOQoBuXtyPSKaFDnEKmH1/r/eRSHAa1vacxu6X9z+6Ck7D2oHT8O9PkUeMwWlYe3Aa+j039ig4OA1r4JbARPV6fejgXl+GnDY05mM0PzAi1Nwiuzb0dKDNjPkULXXIkGvmehKcxrXUIYOumelIcBrXUocMu2aeH8GJhlbPO6EkAicaAicaAicaAicaAicaAicaAicaWn0f9hizv05rf9cApzQCJxoCJxpazQl3YJMInGho6PnyVr83fu6PUwOnNJrG6YvWq8lUY5wunN/ZwLUSOKXRCKcLLwoVTYL+dKhitxf0lt4JTkdrpD85f8DpSI1wuvMXxr0cNBSnpgPcz4qRjFPr5genJMJ1Lg2BEw2BEw2BEw2BEw2BEw2tfj4CnJJok3iN9GZ/ncCJhsCJhsCJhvC8EA2BEw2BEw2BEw2BEw2BEw2BEw2BEw0hTo2GEFdDQ+BEQ+BEQ/g9l4bAiYaGnocVCx2FDzKH0WrglEYDz5f7K0f4gQHtbnBKoqE4ABVQY9d2Ewu7da6YwCmNBjjpdbrs2m5yYTf0p2MUj1Mz56fuil1efnA6Qm5/utwe4JSpvPNTeXuqce8FTpnJ4/Tgd86dtd3kom9BtBo4HaKe69yRtd3A6Qj1cBpZ2w2cjhDuG9EQONEQONEQONEQ4tToCPEaNARONARONAROEARBEARBEARBEARBEJS7qqp+J9XRDSapqk5MaQdOsgXVnByhEZXeUTkJokjxWVks3nVqFW2Ek1CNt7Oy3miqGjl8aV+qlVF5cJpSrO+IPk5VS0KqbhopXm8NzKRWseq8hAWcBrJUS32dE6d6uI22pc6niJu9o1Rfqphsbe1xihfA5nHyzIlnWYwpS07mO+98/2s1bundaq8zbjXNqJqDa/F1VhnfKrv2nva8U4hyWmV8VzE9KLI2wbHD6Ygaes/QaCp2TAxGVnfQE40SBrPuhsokhgBWS780/2vZSP3GVGuUEXKvnxaMrXtwMu19d9/bTXevJqFHNp/T25Rbm++j9aGur3aqq2xHCBJ6DAmHxrqu23KC470GOs6TFUrrgw3t/bfCo7NqZqy2ZakmuXvt0ftzUo1j3Xem7LKjmknTdlXWR91zlEyTXzr5r51HCB5VpSCbs1iYoEvyqhQbtTc0emb7JraqfE41a/uLt6HTK91mA8luy7RK9/Fa4nXT3uk41U6za9Pcyp5b3CN0Tv2vl5PtTxqmR7DWnCrdp/wEe7bzqpR9r3b7U1VVEzi5zquY/tfdeOsOpfuMhVSHe+r3u+1VaTmZcaPlpLfF6O2ewAJO0o0RTvb8JAd1b5ahe4ruTG87cJgEl9Pb5aStMccyxsY5eacNeWoUr+5Gy9HnJAdjzcICPZQT876elXZz5Y575gjjKdVZ+jjpktqTGAsJyP16dFRgDVxv3LNV6q63ipMZ7Oruhk420wdn3LOj39s9MPW4p4uP9CfVYywSbx5R6Q5hOHmjW3v9JAd1tz861ekZVK8dPf1JW2O/JcHhruFB8wwnfYqpuxtmjlGZ3mMJMfP+NrPTcB6RhFMtm2antMalTI97Zvzrzsv1lLb2jjROau9H6DZox+qJdKUxVe922FO7bN3V2zdK5ap8TnZe3k7J2QCnt50KdDf05NqdOwTzcj0ftTNym5aE0yHSvnSGvb303kLtbHzyHah9W5VKLaedMW3MafqNwv+c5gKkiOWG0QAAAABJRU5ErkJggg==)

où :

|     |     |
| --- | --- |
| Élément | Description |
| Format | Format de la page (A3, A4 , A5, etc.) |
| Orientation | Orientation de la page (P ou L) |
| Margins | Marges de la page |
| Units | Unité des marges |
| Top | Valeur de la marge haute |
| Bottom | Valeur de la marge basse |
| Left | Valeur de la marge gauche |
| Right | Valeur de la marge droite |

#### 3.1.3 - Services techniques

Il existe trois services complémentaires :

* un service de génération de fichiers CSV à partir des résultats de recherche fournis par les services d'interrogation
    
* un service de sauvegarde et restauration de contextes de visualisation
    
* un « aiguilleur passe-plats » (proxy applicatif) pour pallier aux problèmes de sécurité des navigateurs vis-à-vis des noms de domaine (problématique SOP, Same Origin Policy)
    

##### 3.1.3.1 - Service de génération de fichiers CSV

L'objectif de ce service est simplement de permettre à l'utilisateur, via son navigateur, d'enregistrer sur son environnement (poste de travail ou ressources réseau) un fichier CSV. Le flux CSV est directement constitué par la bibliothèque JavaScript, le service se chargeant uniquement de transformer ce flux en fichier téléchargeable.

Le service doit être invoqué en POST, le contenu de celui-ci devant comprendre un paramètre CSVcontent qui est tout simplement le flux CSV à transformer.

##### 3.1.3.2 - Service de gestion des contextes de visualisation

Ce service offre un ensemble d'opérations liées au gestionnaire de la bibliothèque JavaScript. Chacune de ces opérations possède son propre protocole.

Sauvegarde d'un contexte

L'appel de cette opération est fait en POST, les paramètres étant :

* view qui est le nom de la vue personnalisée choisie par l'utilisateur
    
* context qui est le flux JSON définissant le contexte de visualisation, tel que décrit au chapitre «[3.5.2 -](#__RefNumPara__33593039) [Ajouter un gestionnaire de contextes de visualisation](#__RefNumPara__33593039) »
    

En retour, le service fourni un flux JSON contenant :

* une propriété contextFile, obligatoire, qui correspond au nom de fichier généré sur le serveur d'application
    
* une propriété maxDate, optionnelle, qui correspond à la date d'expiration du fichier, si le serveur d'application est configuré avec une purge automatique
    

Restauration d'un contexte

L'appel de cette opération est fait en GET, sans aucun paramètre. Le service considère que la chaine (QueryString) transmise dans la requête est le nom du fichier stockant le contexte.

En retour, le service fournit un flux JSON définissant le contexte de visualisation.

Suppression d'un contexte

L'appel de cette opération est fait en GET, le seul paramètre, delete, étant le nom du fichier stockant le contexte à supprimer. Aucun retour particulier n'est fourni par le service.

Vérification de plusieurs contextes

L'appel de cette opération est fait en GET, le seul paramètre, check, étant une chaine comportant les noms de fichiers à vérifier, séparés par des virgules.

En retour, le service fournit une chaine de caractères indiquant les résultats de la vérification. Pour chaque contexte testé, le résultat peut être :

* T : le fichier est disponible, et il ne porte aucune date d'expiration
    
* une date : le fichier est disponible jusqu'à la date indiquée
    
* F : le fichier n'est plus disponible
    

La chaine est construite par concaténation de ces résultats, avec le caractère | comme séparateur.

##### 3.1.3.3 - Aiguilleur passe-plats

Ce service peut relayer des requêtes GET et des requêtes POST.

Dans les deux cas, la chaine transmise dans l'URL de la requête est prise en compte :

* pour les requêtes GET, il s'agit de l'intégralité de la requête à exécuter
    
* pour les requêtes POST, il s'agit de l'identification du serveur à interroger
    

Bien évidemment, dans le cas d'une requête POST, l'intégralité du contenu du POST est transmise au serveur sollicité.

#### 3.1.4 - Configuration des services dans le visualiseur

L'API JavaScript de DESCARTES dispose de propriétés de configuration permettant de lier ses fonctionnalités aux services centralisés nécessaires à leurs fonctionnement.

Chaque application, en fonction de la mise en œuvre des services (cf. chapitres suivants) a la responsabilité de renseigner ces propriétés, comme indiqué dans le tableau suivant :

|     |     |     |
| --- | --- | --- |
| Fonctionnalité | Service centralisé | Propriété de l'objet Descartes |
| Sélection ponctuelle<br><br>Info-bulles | Accès aux données sémantiques WMS | FEATUREINFO_SERVER |
| Sélections surfaciques<br><br>Requêtes attributaires | Accès aux données sémantiques WFS | FEATURE_SERVER |
| Enregistrement PNG | Génération de fichiers PNG | EXPORT_PNG_SERVER |
| Composition PDF | Génération de fichiers PDF | EXPORT_PDF_SERVER |
| Exportation CSV | Génération de fichiers CSV | EXPORT_CSV_SERVER |
| Gestionnaire de contextes | Gestion des fichiers de contexte | CONTEXT_MANAGER_SERVER |
| Découverte de couches WMS | Aiguilleur passe-plats | PROXY_SERVER |

??? note "Historique avant la version 6.0"
	--- 3.2 - Mise en œuvre de la version JAVA

	Les services sont implémentés sous forme de servlet, leur utilisation étant ainsi indépendante de tout framework Java/J2EE.

	---- 3.2.1 - Mise en place du serveur d'application

	Après avoir configuré le classpath du serveur d'application avec les bibliothèques Java nécessaires (celle de DESCARTES et ses dépendances), il suffit de déclarer les servlets adéquates, selon les fonctionnalités souhaitées. Les noms des servlets sont libres, mais ils doivent bien évidemment être cohérents avec la configuration de la bibliothèque JavaScript (cf. chapitre «[5.1.4 -](#__RefNumPara__33601593) [Configuration des services dans le visualiseur](#__RefNumPara__33601593) »).

	Les classes associées aux servlets disponibles sont les suivantes :

	|     |     |
	| --- | --- |
	| Service centralisé | Classe associée à la servlet |
	| Accès aux données sémantiques WMS | GetFeatureInfoServlet |
	| Accès aux données sémantiques WFS | GetFeatureServlet |
	| Génération de fichiers PNG | PngExportMapServlet |
	| Génération de fichiers PDF | PdfExportMapServlet |
	| Génération de fichiers CSV | CsvExportServlet |
	| Gestion des fichiers de contexte | ContextManagerServlet |
	| Aiguilleur passe-plats | BasicProxyServlet |

	!!! note "Remarque"

		Toutes les classes appartiennent au même paquetage, le paquetage fr.gouv.developpementdurable.descartes.services.

	Pour utiliser tous les services centralisés, nous pourrions par exemple rédiger une partie du descripteur de déploiement de l'application ainsi :

	```
	<servlet>
	 <servlet-name>getFeatureInfo</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.GetFeatureInfoServlet</servlet-class>
	</servlet>
	<servlet>
	 <servlet-name>getFeature</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.GetFeatureServlet</servlet-class>
	</servlet>
	<servlet>
	 <servlet-name>exportPDF</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.PdfExportMapServlet</servlet-class>
	</servlet>
	<servlet>
	 <servlet-name>exportPNG</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.PngExportMapServlet</servlet-class>
	</servlet>
	<servlet>
	 <servlet-name>csvExport</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.CsvExportServlet</servlet-class>
	</servlet>
	<servlet>
	 <servlet-name>proxy</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.BasicProxyServlet</servlet-class>
	</servlet>
	<servlet>
	 <servlet-name>contextManager</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.ContextManagerServlet</servlet-class>
	</servlet>
	<servlet-mapping>
	 <servlet-name>getFeatureInfo</servlet-name>
	 <url-pattern>/getFeatureInfo</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
	 <servlet-name>getFeature</servlet-name>
	 <url-pattern>/getFeature</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
	 <servlet-name>exportPDF</servlet-name>
	 <url-pattern>/exportPDF</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
	 <servlet-name>exportPNG</servlet-name>
	 <url-pattern>/exportPNG</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
	 <servlet-name>csvExport</servlet-name>
	 <url-pattern>/csvExport</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
	 <servlet-name>proxy</servlet-name>
	 <url-pattern>/proxy</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
	 <servlet-name>contextManager</servlet-name>
	 <url-pattern>/contextManager</url-pattern>
	</servlet-mapping>
	```

	Cas particulier du service d'accès aux données sémantiques WFS

	Le flux HTML généré par la servlet contient des images pour la localisation rapide des objets sur la carte.

	Par défaut, ces images sont fournies par une servlet complémentaire qui doit donc bien sûr être déclarée si ce service est souhaité.

	```
	...
	<servlet>
	 <servlet-name>getImage</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.tools.imagery.GetImageHelper</servlet-class>
	</servlet>
	...
	<servlet-mapping>
	 <servlet-name>getImage</servlet-name>
	 <url-pattern>/getImage</url-pattern>
	</servlet-mapping>
	...
	```

	!!! note "Remarque"

		La valeur de l'élément url-pattern ne peut être autre que /getImage (contrairement aux autres servlets), sauf à modifier le code de la transformation XSL-T mise en œuvre par le service...

	**Cas particulier du service de gestion des contextes de visualisation**

	Ce service génère des fichiers contenant les définitions des contextes de visualisation. Il est donc indispensable de lui indiquer le dossier de stockage de ces fichiers, grâce au paramètre d'initialisation contexdir.

	Par exemple :

	```
	<servlet>
	 <servlet-name>contextManager</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.ContextManagerServlet</servlet-class>
	 <init-param>
		 <param-name>contextdir</param-name>
		 <param-value>/opt/data/descartes/contextDir</param-value>
	 </init-param>
	</servlet>
	```

	---- 3.2.2 - Paramétrage optionnel des services

	La simple déclaration dans le descripteur de déploiement des services souhaités suffit à leur utilisation.

	Cependant, ils peuvent pour la plupart être personnalisés pour avoir un comportement autre que celui par défaut.

	----- 3.2.2.1 - Service d'accès aux données sémantiques WMS

	Le fonctionnement de ce service repose sur les principales étapes suivantes :

	* appels des requêtes transmises par le protocole de communication
		
	* agrégation par couche DESCARTES des flux XML retournés, dans le cas où il s'agit d'un vrai agrégat
		
	* éventuelle transformation XSL-T des flux retournés vers des flux HTML ou JSON, selon le format de sortie spécifié
		
	* agrégation des flux HTML pour fournir la page résultat
		
	* insertion de styles CSS dans le cas d'un flux correspondant à une page HTML
		

	Dans le cas d'un retour HTML, la transformation XSL-T et la feuille de styles CSS incluses dans le composant sous forme de fichiers peuvent être remplacées par des fichiers propres à une application.

	Deux méthodes sont possibles :

	* Remplacer (ou modifier) les fichiers du composant
		
	* Occulter les fichiers du composant au profit d'autres (inspirés bien sûr des fichiers par défaut)
		

	Le seconde méthode est bien évidemment conseillée.

	Après avoir créé les fichiers propres à l'application, il suffit de déclarer la servlet avec les paramètres d'initialisation XSLfile et stylesCSS, qui référencent ces fichiers avec leur nom et emplacement relatif dans le conteneur du serveur d'application.

	Par exemple :

	```
	<servlet>
		<servlet-name>getFeatureInfo</servlet-name>
		<servlet-class>fr.gouv.developpementdurable.descartes.services.GetFeatureInfoServlet</servlet-class>
		<init-param>
			<param-name>XSLfile</param-name>
			<param-value>WEB-INF/descartes-conf/maTransformation.xsl</param-value>
		</init-param>
		<init-param>
			<param-name>stylesCSS</param-name>
			<param-value>WEB-INF/descartes-conf/mesStyles.css</param-value>
		</init-param>
	</servlet>
	```

	----- 3.2.2.2 - Service d'accès aux données sémantiques WFS

	Le principe de fonctionnement de ce service est identique à celui du service précédent. Les paramètres d'initialisation XSLfile et stylesCSS sont donc aussi possibles.

	Deux paramètres supplémentaires sont disponibles pour substituer les images par défaut pour la localisation rapide des objets sur la carte : imageLocateMenu et imageLocateItem. Ces paramètres indiquent les ressources HTTP correspondant à ces images, de manière absolue ou relative à l'application.

	Par exemple :

	```
	<servlet>
		<servlet-name>getFeature</servlet-name>
		<servlet-class>fr.gouv.developpementdurable.descartes.services.GetFeatureServlet</servlet-class>
		<init-param>
			<param-name>XSLfile</param-name>
			<param-value>WEB-INF/descartes-conf/monAutreTransformation.xsl</param-value>
		</init-param>
		<init-param>
			<param-name>stylesCSS</param-name>
			<param-value>WEB-INF/descartes-conf/mesAutresStyles.css</param-value>
		</init-param>
		<init-param>
			<param-name>imageLocateMenu</param-name>
			<param-value>images/menuLocalisation.png</param-value>
		</init-param>
		<init-param>
			<param-name>imageLocateItem</param-name>
			<param-value>images/objetLocalisation.png</param-value>
		</init-param>
	</servlet>
	```

	----- 3.2.2.3 - Service de génération de documents PDF

	Tout d'abord, un paramètre d'initialisation est requis dans le cas où les logos devant être insérés dans le document PDF sont fournis directement par le serveur applicatif (présence d'éléments FichierLogo dans le flux de communication) et non récupérés par une URL.

	Pour des raisons de sécurité, seuls les éléments discriminants pour retrouver le fichier sont en effet transmis. L'application doit connaître le répertoire de stockage des fichiers accessibles, grâce au paramètre d'initialisation repertoireLogos qui indique le chemin d'accès absolu à ce répertoire.

	Par exemple :

	```
	<servlet>
		<servlet-name>exportPDF</servlet-name>
		<servlet-class>fr.gouv.developpementdurable.descartes.services.PdfExportMapServlet</servlet-class>
		<init-param>
			<param-name>repertoireLogos</param-name>
			<param-value>/opt/logos/</param-value>
		</init-param>
	</servlet>
	```

	Par ailleurs, il est possible de générer le document PDF selon plusieurs « formats de papier ».  La bibliothèque utilisée, iText, dispose en interne d'un nombre important de formats. Il est toutefois possible de configurer le service avec d'autres formats, en cohérence toutefois avec la bibliothèque JavaScript (cf. chapitre «[4.2.1.3 -](#__RefNumPara__33611594) [Les formats de papier](#__RefNumPara__33611594) »).

	Pour cela, il faut :

	* définir, dans un fichier « properties » dédié, les nouveaux formats, grâce à deux lignes conformes au patron suivant :
		
		descartes.paper.<nom_du_format>.h
		
		descartes.paper.<nom_du_format>.w
		
		Ces deux lignes indiquent les dimensions en millimètres en mode portrait.
		
		Par exemple :
		
	```
	descartes.paper.perso.h=600
	descartes.paper.perso.w=400
	```

	* référencer ce fichier dans le descripteur de déploiement grâce au paramètre d'initialisation extraPapers :
		
	```
	<servlet>
	 <servlet-name>exportPDF</servlet-name>
	 <servlet-class>fr.gouv.developpementdurable.descartes.services.PdfExportMapServlet</servlet-class>
	 <init-param>
		 <param-name>extraPapers</param-name>
		 <param-value>WEB-INF/papers.properties</param-value>
	 </init-param>
	</servlet>
	```

	----- 3.2.2.4 - Service de gestion des contextes de visualisation

	Si le serveur d'application met en place un mécanisme de purge périodique des fichiers de contexte, le service doit connaître cette périodicité pour correctement effectuer son opération de vérification. Le paramètre d'initialisation maxDays permet d'indiquer le nombre maximal de jours de disponibilité des fichiers :

	Par exemple :

	```
	<servlet>
		<servlet-name>contextManager</servlet-name>
		<servlet-class>fr.gouv.developpementdurable.descartes.services.ContextManagerServlet</servlet-class>
		<init-param>
			<param-name>contextdir</param-name>
			<param-value>/opt/data/descartes/contextDir</param-value>
		</init-param>
		<init-param>
			<param-name>maxDays</param-name>
			<param-value>5</param-value>
		</init-param>
	</servlet>
	```

	---- 3.2.3 - Paramétrage général complémentaire

	----- 3.2.3.1 - Prise en compte des appels à des serveurs externes

	Afin de permettre au serveur d'application d'accéder à des ressources externes, il convient de renseigner les paramètres de l'éventuel proxy local utilisé.

	Les paramètres de contexte suivants doivent alors être renseignés dans le descripteur de déploiement :

	* proxyHost : nom d'hôte du proxy
		
	* proxyPort : port du proxy
		
	* proxyExceptions : exceptions pour le proxy
		

	Par exemple :

	```
	<context-param>
		<param-name>proxyHost</param-name>
		<param-value>proxy.xxx.i2</param-value>
	</context-param>
	<context-param>
		<param-name>proxyPort</param-name>
		<param-value>8080</param-value>
	</context-param>
	<context-param>
		<param-name>proxyExceptions</param-name>
		<param-value>localhost;.i2</param-value>
	</context-param>
	```

	----- 3.2.3.2 - Prise en compte des contraintes de sécurité en centre-serveur

	Les contraintes de sécurité en centre-serveur impliquent souvent que :

	* Le serveur d'application est installé dans la « zone interne » du centre-serveur
		
	* Aucun appel hors de cette zone interne n’est permis
		

	Étant donné que les requêtes transmises aux servlets sont en fait des « adresses externes », il est indispensable de paramétrer les correspondances entre celles-ci et les « adresses internes » grâce à un fichier référencé par le paramètre de contexte hostmapping, fichier dont le nom est libre.

	Par exemple :

	```
	<context-param>
		<param-name>hostmapping</param-name>
		<param-value>WEB-INF/descartes-conf/hostmapping.xml</param-value>
	</context-param>
	```

	Le contenu de ce fichier doit correspondre à une instance du schéma XML suivant :

	![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZsAAAB6CAMAAABeKQ5ZAAAAPFBMVEX///8AAADAwMDMzMxERER3d3cRERGqqqpmZmZVVVUzMzO7u7vd3d2ZmZkiIiLu7u6IiIj/vr7+b3D/AACnNhOYAAAH8UlEQVR4nO2ciYLqKBBFYeLWGmND/v9fh6VYA9mEyLPr9jxNs1XBCQXaYQhBoVAoFAqFQq0V3alP+/0XtHOQkc0BQjbtCtm0q71s/hMq6wkqFrJpV5aN23p1pxXVkE19Jdis2SAjmwPkE6H0TC9X/enlRumPTLzfIdkkQWlkU18hm0dPL0Sj6Xt6EpcdMcmQBKWRTQXlPuArNt6bylPZNtkrjmxqiGZ+jdmYROInu2rIpoLWsfmhTx3TiGUDSVAa2VTQMpvrhT7lwn8K5w0kQWlkU0E5NhtbQTYVhGzaFbJpV8imXSGbdoVs2lXMZqeQTQUlJsp/O3W8818uZNOukE27QjbtCp+QaVfIpl0hm3aFbNoVsmlXyKZdIZt29XE2e7/BW6VNBgu6WMhuqZFaw2GFA0WVanv1UKypcqjd44f4YDZz5nJ5JdgUsPvtbOatZXK3s5nEkRJ2v4xN/DXfkrF0/p55Q+OE9+3uH+K933ZWYpNcCZdt7ax2iN0PsvGe7XXq1BOLtCd9bruRS05crRmj2WpZFyvbnbjhBqPq6aj5jsOD8k/y3LgVPI5NmOA14MajOJuc9UzjxdjcKL0R8rhTeurVDULpcCKnQVz1gzz5Iw8ADb17MyeC+hO9uZMNqR6WYjNxMdlAXTY9vZM7zKBap6PCmUpFo9er6PmFvqR1PRpnMf5ncSlRyANBXacPA+k3cyLoh3aP1WwozEh4mZZIs8m4mGwgyWaX3ZQb5EzFvVj3dFR8U8KbvCefJvFBf6kc9tf5Pjl7kjyMIps1S2v8AXn203O+2qyLfncK2k0UNG4IiUjyqns6KtNx0p0HOnim5NtAu/VsJgZKzJuMi8kGKs4b9XaRE8XrdMjGr1iKDQSMO4X4pRJPJnj16vXZiWBq30whEdOeh6w3UxeTDVTeC9zE6qqPRFU7HZVZaF8/Ymnr1eEfkfhLf2XeVS32cv059e7N1BQ7hfORewHPxWQDb7PJlVHxSq61J9pVPR217PekBvXfnC5P8hIxJm62IJs1zrmramy2qgE2j0Hdx6bQZAldNUb7qiUb2MQmZTcus8GNsGJj39notjd/n5Z80G6zizQ2XcTul7NZsEbTD0G+z6aI3W9nM2uOZh5QLcCmhN2vZzO31uYeHi7BZo/dd90IDOyqWFPJkZ4rnOzFdrvJcLrZbrGReudp9fzz7wW01Vp6XJbGbZIwbWqP3XIjtar5ohZ3ObWjD4vxJC6Qule32k0YLTkM67Tf4j6ntnehTBzZaPdPsskbfMNL6r3mWttmtwk2/4aWek9jNF8+Hi1p8dakERpkc5iW40aEBtl8r2p+LEe9J2TzlqoOH7J5RzN/bUF9VjR8SiN8PAP1YQV/S1bfcVa0pZ8H2Pb6hxWyIWXZlPr7zR9V1XmDMN5S1fUG2bwlCi9NDiP+HwcP0/Y/jSObo+TYPAc6XAnp7/BA3l0+tZ6ogGyqKbdPe9AbudEHGeRBjoGoM33XVAPIpppyQewuZslLPZyun2I90x93EMoXsqmmHBvzUPqZXm6SzuXyopdUQWRTTblP+fbAQHe70F8R4s5iKnWJ+sjmKFk4Iqb9PODEhDprI3VOVEA2R8myudLbk4oJA3sBedDmSu+JCsjmKLmg9jvQ+6WDPfRTUBJbg9QuGtl8Rn1yZxYK2bQrZNOscL05TPh9WrtCNu1q+98hkM2B2vi0ALI5UsvP/cb6tMd/R8imXSGbdoVsUCgUCoVCoVAoFAqFQqFQqEUxxsdD9ekO/zNi/GAyhdgor9mWGrFhBgnMy5BNyt+1l/IVclnWcS+DLfeN2REQphaK750zXDv1OTZrmgo7n2LD3OgrcdEx+W8ESCaX5cwFGTvYzFRhe8f302z4fL9s77zfMkMblNJzhhHVQx6wyTdAtrEJ3MlX2Y2mGTbm3vbuc65jEiTrVC8mCdeZKMzlbasrjro6jBiMtteIHihmxosRCHjEZXh+eBMOQCfCnjHsuRhFTT+gyU5Jh8n0QleSU51wNS7iP646CS9E90Y7oVLDvChulmJj+jhOX92lnwqjD1ErZDOadrm57+y4gT3umWP2ho8yEo7EYY9z7tqJygcd9AZMGVTeRxcw4qNGAlWBE+G2Ld0lP9WWrsNGd4hMX4n2xUYskwe+MDsu0zVH5ambS/24vYBkwJgGa1alOANaCkzKCx6EvcDt0EUnFrLhxM2L4ALyGfTZgLHXKo/BXOYKqZ831mXDva5y00Vm1wq/BNSEnyQbO28AYECNAxsGcyfMsKtXYFLNMe7PG8bYCjb+gDECP9OLESYOzA0LhscpfBzd7KnPxsQExwauZTT2F6SIjRq6DBu73qggHewUYEbApBltUDAZPpvRZwPemLKEkGU2wTKgljr5b3rh2IVsVKCF8bcQD2dDgtuQwdAyP6aZEmZ09KRIsYGW3KJE4lFX6RD5NEwDNIhp1iRMsbfYmEDGpxeQbbYAXkyzkW30Cx4R06DJzLzRM8NiCPYCDG58wyaIXO7zjQrS/rzzzMHOJ+lHYt6AN/bOiIr7jkfdM2xgyeDTC7NPYGaWWCrEvI5mVxnvBaqx4ao7dvtphpFATDOxbbqHhu0nD0qagXHfC4DfMJiw6WWAho0upOkka5uNoVO6FgvZ2D202z6TGTajXc6nF7AR9tf/aA8N+0i7e7Z51dh8RDB+XkirpbGE3M559bc/dXtVU45NZTSF2az/Yu5/bizuGQcaQccAAAAASUVORK5CYII=)

	Par exemple :

	```
	<?xml version="1.0" encoding="UTF-8"?>
	<HostMapping>
		<Host>
			<Interne>mapserver1.app.sen.centre-serveur.i2</Interne>
			<Externe>mapserveur.application.developpement-durable.gouv.fr</Externe>
		</Host>
		<Host>
			<Interne>georef.app.sen.centre-serveur.i2/cartes/mapserv</Interne>
			<Externe>georef.application.i2/cartes/mapserv</Externe>
			<QSA>map=/opt/data/carto/ref/ms/glissant/refs.i2.map</QSA>
		</Host>
	</HostMapping>
	```

	--- 3.3 - Mise en œuvre de la version PHP

	---- 3.3.1 - Mise en place du serveur d'application

	Pour une application PHP, il suffit de déposer l'arborescence des scripts PHP de la bibliothèque dans un dossier judicieux (par exemple php_lib).

	Les points d'entrées des services sont des scripts présents dans le dossier services de la bibliothèque :

	|     |     |
	| --- | --- |
	| Service centralisé | Script PHP |
	| Accès aux données sémantiques WMS | services/getFeatureInfo |
	| Accès aux données sémantiques WFS | services/getFeature |
	| Génération de fichiers PNG | services/exportPNG |
	| Génération de fichiers PDF | services/exportPDF |
	| Génération de fichiers CSV | services/exportCSV |
	| Gestion des fichiers de contexte | services/contextManager |
	| Aiguilleur passe-plats | services/proxy |

	!!! note "Remarque"

		Ces points d'entrée sont ceux à renseigner lors de la configuration de la bibliothèque JavaScript (cf. chapitre «[5.1.4 -](#__RefNumPara__33601593) [Configuration des services dans le visualiseur](#__RefNumPara__33601593) »).

	Les diverses options de paramétrage sont centralisées dans le fichier descartes.properties présent à la racine de la bibliothèque PHP, grâce à un système classique de clés/valeurs.

	Cas particulier du service de gestion des contextes de visualisation

	Ce service génère des fichiers contenant les définitions des contextes de visualisation. Il est donc indispensable de lui indiquer le dossier de stockage de ces fichiers, grâce à la clé de paramétrage contexdir.

	Par exemple :

	```
	contextdir=/opt/data/descartes/contextDir
	```

	Cas particulier des services de génération de fichiers PNG et PDF

	La clé export.repertoireTemporaire est indispensable pour stocker les fichiers temporaires générés par les services.

	Par exemple :

	```
	exportPDF.repertoireTemporaire=/tmp/
	```

	---- 3.3.2 - Paramétrage optionnel des services

	Le simple dépôt de l'arborescence des scripts PHP de la bibliothèque suffit à l'utilisation des services souhaités.

	Cependant, ils peuvent pour la plupart être personnalisés pour avoir un comportement autre que celui par défaut.

	----- 3.3.2.1 - Service d'accès aux données sémantiques WMS

	Le fonctionnement de ce service repose sur les principales étapes suivantes :

	* appels des requêtes transmises par le protocole de communication
		
	* agrégation par couche DESCARTES des flux XML retournés, dans le cas où il s'agit d'un vrai agrégat
		
	* éventuelle transformation XSL-T des flux retournés vers des flux HTML ou JSON, selon le format de sortie spécifié
		
	* agrégation des flux HTML pour fournir la page résultat
		
	* insertion de styles CSS dans le cas d'un flux correspondant à une page HTML
		

	Dans le cas d'un retour HTML, la transformation XSL-T et la feuille de styles CSS incluses dans le composant sous forme de fichiers peuvent être remplacées par des fichiers propres à une application.

	Deux méthodes sont possibles :

	* Remplacer (ou modifier) les fichiers du composant
		
	* Occulter les fichiers du composant au profit d'autres (inspirés bien sûr des fichiers par défaut)
		

	Le seconde méthode est bien évidemment conseillée.

	Après avoir créé les fichiers propres à l'application, il suffit de renseigner les clés getFeatureInfo.transform et getFeatureInfo.styles du fichier de configuration, qui référencent ces fichiers avec leur nom et emplacement relatif dans le conteneur du serveur d'application.

	Par exemple :

	```
	getFeatureInfo.transform=../../descartes-conf/maTransformation.xsl
	getFeatureInfo.styles=../../descartes-conf/mesStyles.css
	```

	----- 3.3.2.2 - Service d'accès aux données sémantiques WFS

	Le principe de fonctionnement de ce service est identique à celui du service précédent. Les clés getFeature.transform et getFeature.styles permettent de la même façon de personnaliser le service.

	Deux autres clés sont disponibles pour substituer les images par défaut pour la localisation rapide des objets sur la carte : getFeature.imageLocateMenu et getFeature.imageLocateItem. Ces clés indiquent les ressources HTTP correspondant à ces images, de manière absolue ou relative à l'application.

	Par exemple :

	```
	getFeature.transform=../../descartes-conf/monAutreTransformation.xsl
	getFeature.styles=../../descartes-conf/mesAutresStyles.css
	imageLocateMenu=images/menuLocalisation.png
	imageLocateItem=images/objetLocalisation.png
	```

	----- 3.3.2.3 - Service de génération de documents PDF

	Tout d'abord, une clé est requise dans le cas où les logos devant être insérés dans le document PDF sont fournis directement par le serveur applicatif (présence d'éléments FichierLogo dans le flux de communication) et non récupérés par une URL.

	Pour des raisons de sécurité, seuls les éléments discriminants pour retrouver le fichier sont en effet transmis. L'application doit connaître le répertoire de stockage des fichiers accessibles, grâce à la clé exportPDF.repertoireLogos qui indique le chemin d'accès absolu à ce répertoire.

	Par exemple :

	```
	exportPDF.repertoireLogos=/opt/logos/
	```

	Par ailleurs, il est possible de générer le document PDF selon plusieurs « formats de papier ».  La bibliothèque utilisée, fPdf, dispose en interne de quelques formats. Il est toutefois possible de configurer le service avec d'autres formats, en cohérence toutefois avec la bibliothèque JavaScript (cf. chapitre «[4.2.1.3 -](#__RefNumPara__33611594) [Les formats de papier](#__RefNumPara__33611594) »).

	Pour cela, il faut définir, dans le fichier de paramétrage, les nouveaux formats, grâce à deux lignes conformes au patron suivant :

	* descartes.paper.<nom_du_format>.h
		
	* descartes.paper.<nom_du_format>.w
		

	Ces deux lignes indiquent les dimensions en millimètres en mode portrait.

	Par exemple :

	```
	descartes.paper.perso.h=600
	descartes.paper.perso.w=400
	```

	----- 3.3.2.4 - Service de gestion des contextes de visualisation

	Si le serveur d'application met en place un mécanisme de purge périodique des fichiers de contexte, le service doit connaître cette périodicité pour correctement effectuer son opération de vérification. La clé maxDays permet d'indiquer le nombre maximal de jours de disponibilité des fichiers :

	Par exemple :

	```
	maxDays=5
	```

	---- 3.3.3 - Paramétrage général complémentaire

	----- 3.3.3.1 - Prise en compte des appels à des serveurs externes

	Afin de permettre au serveur d'application d'accéder à des ressources externes, il convient de renseigner les paramètres de l'éventuel proxy local utilisé.

	Les clés suivantes doivent alors être renseignées dans le descripteur de déploiement :

	* global.proxy.host : nom d'hôte du proxy
		
	* global.proxy.port : port du proxy
		
	* global.proxy.exceptions : exceptions pour le proxy
		

	Par exemple :

	```
	global.proxy.host=proxy.xxxx.i2
	global.proxy.port=8080
	global.proxy.exceptions=localhost;.i2
	```

	----- 3.3.3.2 - Prise en compte des contraintes de sécurité en centre-serveur

	Les contraintes de sécurité en centre-serveur impliquent souvent que :

	* Le serveur d'application est installé dans la « zone interne » du centre-serveur
		
	* Aucun appel hors de cette zone interne n’est permis
		

	Étant donné que les requêtes transmises aux scripts sont en fait des « adresses externes », il est indispensable de paramétrer les correspondances entre celles-ci et les « adresses internes » grâce à un fichier référencé par la clé global.hostmapping, fichier dont le nom est libre.

	Par exemple :

	global.hostmapping=WEB-INF/descartes-conf/hostmapping.xml

	Le contenu de ce fichier doit correspondre à une instance du schéma XML suivant :

	![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZsAAAB6CAMAAABeKQ5ZAAAAPFBMVEX///8AAADAwMDMzMxERER3d3cRERGqqqpmZmZVVVUzMzO7u7vd3d2ZmZkiIiLu7u6IiIj/vr7+b3D/AACnNhOYAAAH8UlEQVR4nO2ciYLqKBBFYeLWGmND/v9fh6VYA9mEyLPr9jxNs1XBCQXaYQhBoVAoFAqFQq0V3alP+/0XtHOQkc0BQjbtCtm0q71s/hMq6wkqFrJpV5aN23p1pxXVkE19Jdis2SAjmwPkE6H0TC9X/enlRumPTLzfIdkkQWlkU18hm0dPL0Sj6Xt6EpcdMcmQBKWRTQXlPuArNt6bylPZNtkrjmxqiGZ+jdmYROInu2rIpoLWsfmhTx3TiGUDSVAa2VTQMpvrhT7lwn8K5w0kQWlkU0E5NhtbQTYVhGzaFbJpV8imXSGbdoVs2lXMZqeQTQUlJsp/O3W8818uZNOukE27QjbtCp+QaVfIpl0hm3aFbNoVsmlXyKZdIZt29XE2e7/BW6VNBgu6WMhuqZFaw2GFA0WVanv1UKypcqjd44f4YDZz5nJ5JdgUsPvtbOatZXK3s5nEkRJ2v4xN/DXfkrF0/p55Q+OE9+3uH+K933ZWYpNcCZdt7ax2iN0PsvGe7XXq1BOLtCd9bruRS05crRmj2WpZFyvbnbjhBqPq6aj5jsOD8k/y3LgVPI5NmOA14MajOJuc9UzjxdjcKL0R8rhTeurVDULpcCKnQVz1gzz5Iw8ADb17MyeC+hO9uZMNqR6WYjNxMdlAXTY9vZM7zKBap6PCmUpFo9er6PmFvqR1PRpnMf5ncSlRyANBXacPA+k3cyLoh3aP1WwozEh4mZZIs8m4mGwgyWaX3ZQb5EzFvVj3dFR8U8KbvCefJvFBf6kc9tf5Pjl7kjyMIps1S2v8AXn203O+2qyLfncK2k0UNG4IiUjyqns6KtNx0p0HOnim5NtAu/VsJgZKzJuMi8kGKs4b9XaRE8XrdMjGr1iKDQSMO4X4pRJPJnj16vXZiWBq30whEdOeh6w3UxeTDVTeC9zE6qqPRFU7HZVZaF8/Ymnr1eEfkfhLf2XeVS32cv059e7N1BQ7hfORewHPxWQDb7PJlVHxSq61J9pVPR217PekBvXfnC5P8hIxJm62IJs1zrmramy2qgE2j0Hdx6bQZAldNUb7qiUb2MQmZTcus8GNsGJj39notjd/n5Z80G6zizQ2XcTul7NZsEbTD0G+z6aI3W9nM2uOZh5QLcCmhN2vZzO31uYeHi7BZo/dd90IDOyqWFPJkZ4rnOzFdrvJcLrZbrGReudp9fzz7wW01Vp6XJbGbZIwbWqP3XIjtar5ohZ3ObWjD4vxJC6Qule32k0YLTkM67Tf4j6ntnehTBzZaPdPsskbfMNL6r3mWttmtwk2/4aWek9jNF8+Hi1p8dakERpkc5iW40aEBtl8r2p+LEe9J2TzlqoOH7J5RzN/bUF9VjR8SiN8PAP1YQV/S1bfcVa0pZ8H2Pb6hxWyIWXZlPr7zR9V1XmDMN5S1fUG2bwlCi9NDiP+HwcP0/Y/jSObo+TYPAc6XAnp7/BA3l0+tZ6ogGyqKbdPe9AbudEHGeRBjoGoM33XVAPIpppyQewuZslLPZyun2I90x93EMoXsqmmHBvzUPqZXm6SzuXyopdUQWRTTblP+fbAQHe70F8R4s5iKnWJ+sjmKFk4Iqb9PODEhDprI3VOVEA2R8myudLbk4oJA3sBedDmSu+JCsjmKLmg9jvQ+6WDPfRTUBJbg9QuGtl8Rn1yZxYK2bQrZNOscL05TPh9WrtCNu1q+98hkM2B2vi0ALI5UsvP/cb6tMd/R8imXSGbdoVsUCgUCoVCoVAoFAqFQqFQqEUxxsdD9ekO/zNi/GAyhdgor9mWGrFhBgnMy5BNyt+1l/IVclnWcS+DLfeN2REQphaK750zXDv1OTZrmgo7n2LD3OgrcdEx+W8ESCaX5cwFGTvYzFRhe8f302z4fL9s77zfMkMblNJzhhHVQx6wyTdAtrEJ3MlX2Y2mGTbm3vbuc65jEiTrVC8mCdeZKMzlbasrjro6jBiMtteIHihmxosRCHjEZXh+eBMOQCfCnjHsuRhFTT+gyU5Jh8n0QleSU51wNS7iP646CS9E90Y7oVLDvChulmJj+jhOX92lnwqjD1ErZDOadrm57+y4gT3umWP2ho8yEo7EYY9z7tqJygcd9AZMGVTeRxcw4qNGAlWBE+G2Ld0lP9WWrsNGd4hMX4n2xUYskwe+MDsu0zVH5ambS/24vYBkwJgGa1alOANaCkzKCx6EvcDt0EUnFrLhxM2L4ALyGfTZgLHXKo/BXOYKqZ831mXDva5y00Vm1wq/BNSEnyQbO28AYECNAxsGcyfMsKtXYFLNMe7PG8bYCjb+gDECP9OLESYOzA0LhscpfBzd7KnPxsQExwauZTT2F6SIjRq6DBu73qggHewUYEbApBltUDAZPpvRZwPemLKEkGU2wTKgljr5b3rh2IVsVKCF8bcQD2dDgtuQwdAyP6aZEmZ09KRIsYGW3KJE4lFX6RD5NEwDNIhp1iRMsbfYmEDGpxeQbbYAXkyzkW30Cx4R06DJzLzRM8NiCPYCDG58wyaIXO7zjQrS/rzzzMHOJ+lHYt6AN/bOiIr7jkfdM2xgyeDTC7NPYGaWWCrEvI5mVxnvBaqx4ao7dvtphpFATDOxbbqHhu0nD0qagXHfC4DfMJiw6WWAho0upOkka5uNoVO6FgvZ2D202z6TGTajXc6nF7AR9tf/aA8N+0i7e7Z51dh8RDB+XkirpbGE3M559bc/dXtVU45NZTSF2az/Yu5/bizuGQcaQccAAAAASUVORK5CYII=)

	Par exemple :

	```
	<?xml version="1.0" encoding="UTF-8"?>
	<HostMapping>
		<Host>
			<Interne>mapserver1.app.sen.centre-serveur.i2</Interne>
			<Externe>mapserveur.application.developpement-durable.gouv.fr</Externe>
		</Host>
		<Host>
			<Interne>georef.app.sen.centre-serveur.i2/cartes/mapserv</Interne>
			<Externe>georef.application.i2/cartes/mapserv</Externe>
			<QSA>map=/opt/data/carto/ref/ms/glissant/refs.i2.map</QSA>
		</Host>
	</HostMapping>
	```