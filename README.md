# Documentation Descartes

Ce projet contient la documentation  Descartes.

La documentation est rédigée en [markdown]((https://www.markdownguide.org/)) et produite en html à l'aide de [Mkdocs](https://www.mkdocs.org/).
Mkdocs possède quelques [particularités markdown](https://www.markdownguide.org/tools/mkdocs/).

## Accès à la documentation en ligne

https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-docs

## Installation

Les composants à installer sont déclarés dans le fichier [requirements.txt](requirements.txt) avec notamment mkdocs, la localisation française et le [thème *Material*](https://squidfunk.github.io/mkdocs-material/) pour l'extension [*Adminitions*](https://squidfunk.github.io/mkdocs-material/reference/admonitions/).

```
$ pip install -r requirements.txt
```

## Utilisation

Pour contrôler en direct la documentation pendant son édition avec le serveur intégré de mkdocs :

```
$ mkdocs serve
```

Url d'accès: http://localhost:8000

## Configuration des variables d'environnements

Listes des variables disponibles:
* MAJ_DATE

```
$ export MA_VARIABLE=XXXX
$ mkdocs serve
```
